﻿﻿using System;
using System.Resources;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using HostedMobile.Core.ResX;
using System.Windows.Input;
using System.Linq;
using System.Reactive.Linq;

using Xamarin.Forms;

namespace HostedMobile
{
    public class ScheduleViewModel : BaseViewModel
    {
        public ObservableRangeCollection<Match> MatchList { get; set; }
        public ObservableRangeCollection<OtherMatches> OtherMatchList { get; set; }
		public ObservableRangeCollection<String> Comps { get; set; }
        public Command LoadMatchesCommand { get; set; }
        public string CurrentCompName { get; set; }
        public List<Competition> CompList;
        public List<String> compList;
        public int roundIndex;
        public ObservableRangeCollection<PoolDetail> PoolList{ get; set; }
        public ObservableRangeCollection<PhaseDetail> PhaseList{ get; set; }
        public ObservableRangeCollection<MatchType> MatchTypeList{ get; set; }
        public ObservableRangeCollection<RoundDetail> RoundList { get; set; }
        public ObservableRangeCollection<String> DateList { get; set; }
        public DateTime ScheduleUpdated;

        ICommand poolCommand;
        ICommand phaseCommand;
        ICommand matchTypeCommand;
        ICommand changeRound;

		public ScheduleViewModel()
        {
            Title = AppResources.ResourceManager.GetString("Schedule");
            MatchList = new ObservableRangeCollection<Match>();
            OtherMatchList = new ObservableRangeCollection<OtherMatches>();
			PoolList = new ObservableRangeCollection<PoolDetail>();
            PhaseList = new ObservableRangeCollection<PhaseDetail>();
            RoundList = new ObservableRangeCollection<RoundDetail>();
            MatchTypeList = new ObservableRangeCollection<MatchType>();
            Comps = new ObservableRangeCollection<String>();
            DateList = new ObservableRangeCollection<String>();
            LoadMatchesCommand = new Command(async () => await ExecuteLoadMatchesCommand());
            poolCommand = new Command(PoolTapped);
            phaseCommand = new Command(PhaseTapped);
            matchTypeCommand = new Command(MatchTypeTapped);
            changeRound = new Command(ChangeRoundTap);
			roundIndex = new int();
        }

        async Task ExecuteLoadMatchesCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            IsNotBusy = false;

            try
            {
                //SameComp = CurrentCompName == Settings.CurrentComp ? true : false;

                int r;
				bool isRoundNumeric = int.TryParse(Settings.CurrentRound, out r);
                if (RoundList.Count()>1  && isRoundNumeric != true) {
					int tempRoundIndex = -1;
					for (int i = 0; i < RoundList.Count; i++)
					{
						if (String.Equals(RoundList[i].name, Settings.CurrentRound, StringComparison.OrdinalIgnoreCase))
						{
							tempRoundIndex = i;
							break;
						}
					}
					int args = 1;
					string stuff = (tempRoundIndex + args).ToString();
                    Settings.CurrentRound = stuff;
                }

				var hs = new HostedDataService();

                var matchList = await hs.GetMatches(true);

                Settings.CurrentComp = matchList.competitionId;
                var otherMatchList = await hs.GetOtherMatches(true);

                double n;
                if (double.TryParse(matchList.current_roundNumber, out n))
                {
                    //int roundInd = Convert.ToInt16(matchList.current_roundNumber) - 1;
					int roundInd = matchList.roundDetails.FindIndex(a => a.number == matchList.current_roundNumber);
                    if (matchList.roundDetails.Count > 1)
                    {
						Settings.CurrentRoundName = matchList.roundDetails[roundInd].name;
						Settings.CurrentRound = matchList.current_roundNumber;
                    }
                    else
                    {
                        Settings.CurrentRound = matchList.current_roundNumber;
                    }

                }
                else
                {
                    Settings.CurrentRound = matchList.current_roundNumber;
                }
                Settings.CurrentPhase = matchList.current_phaseName;
                Settings.CurrentMatchType = matchList.current_matchType;
                Settings.CurrentPool = matchList.current_poolNumber.ToString();

                foreach (var match in matchList.matches)
                {
					Competitor homeCompetitor = new Competitor();
					Competitor awayCompetitor = new Competitor();
					if (match != null && match.competitors.Count() > 0)
                    {

                        string noCompetitiorText = "";
                        if (match.matchStatus == "BYE")
                        {
                            noCompetitiorText = AppResources.BYE;
                        }
                        else
                        {
                            noCompetitiorText = AppResources.TBD;
                        }

                        foreach (var competitor in match.competitors){
                            if(competitor.isHomeCompetitor == 1 || match.matchStatus == "BYE" ){
                                homeCompetitor = competitor;
                            } else {
                                awayCompetitor = competitor;
                            }
                        }

                        if (homeCompetitor.clubId == 0){
                            homeCompetitor.clubName = noCompetitiorText;
						    homeCompetitor.teamName = noCompetitiorText;
						}
						if (awayCompetitor.clubId == 0)
						{
							awayCompetitor.clubName = noCompetitiorText;
							awayCompetitor.teamName = noCompetitiorText;
						}


                    } else {
						homeCompetitor.clubName = "";
						homeCompetitor.teamName = "";
						awayCompetitor.clubName = "";
						awayCompetitor.teamName = "";

					}
					match.competitors.Clear();
					match.competitors.Add(homeCompetitor);
					match.competitors.Add(awayCompetitor);
                    match.IsPostponed = false;

                    if (match.matchStatus == "POSTPONED" || match.matchStatus == "CANCELLED" || match.matchStatus == "ABANDONED")
                    {
                        match.IsPostponed = true;
                        match.postponedStatus = AppResources.ResourceManager.GetString(match.matchStatus);
                    }
				}
                if (matchList.dates != null)
                {
                    //DateSelectedIndex = 0;
                    var dateSelected = new int();
                    int ds = 0;
                    List<string> tempDates = new List<String>();
                    foreach (String dateString in matchList.dates)
                    {
                        if (dateString == matchList.dateFilter)
                        {
                            Settings.CurrentDate = dateString;
                            dateSelected = ds;
                        }
                        DateTime tempDate = DateTime.Parse(dateString);
                        tempDates.Add(tempDate.ToString("ddd, dd MMMM yyyy"));
                        ds++;
                    }

                    if (DateList.Count == 0)
                    {
                        DateList.Clear();
                        DateList.ReplaceRange(tempDates);
                        DateSelectedIndex = dateSelected;

                    }

                    if (!SameComp || DateList.Count != matchList.dates.Count)
                    {
                        int tempCount = DateList.Count();
                        DateList.AddRange(tempDates);
                        for (int dr = 0; dr < tempCount; dr++)
                        {
                            DateList.RemoveAt(0);
                        }
                        DateSelectedIndex = dateSelected;
                    }
                    HasDates = matchList.dates.Count > 1 ? true : false;
                }
                var dataMatch = Compare.JsonCompare(MatchList, matchList.matches);
                if (!dataMatch)
                {
                    MatchList.Clear();
                    MatchList.ReplaceRange(matchList.matches);
                    PoolList.Clear();
                    PoolList.ReplaceRange(matchList.poolDetails);
                    PhaseList.Clear();
                    PhaseList.ReplaceRange(matchList.phases);
                    MatchTypeList.Clear();
                    MatchTypeList.ReplaceRange(matchList.matchTypes);
                    CurrentRound = Settings.CurrentRound;
                    SameComp = true;
                }
                CompList = matchList.FilterLists.competitions;
                //Load Comp Picker
                if (Comps.Count == 0)
                {
                    Comps.Clear();
                    compList = hs.GetFilterComps(matchList.FilterLists);
                    Comps.ReplaceRange(compList);
                }
                CompSelectedIndex = hs.GetFilterCompsSelected(matchList.FilterLists, matchList.competitionId);

                RoundList.Clear();
                RoundList.ReplaceRange(matchList.roundDetails);
                int roundIndex = matchList.roundDetails.FindIndex(p => p.name == Settings.CurrentRoundName);

                HasRounds = matchList.roundDetails.Count > 1 ? true : false;
                HasPools = matchList.poolDetails.Count > 1 ? true : false;
                HasPhases = matchList.phases.Count > 1 ? true : false;
                HasMatchTypes = matchList.matchTypes.Count > 1 ? true : false;

                ShowNextRound = roundIndex + 1 != matchList.roundDetails.Count ? true : false;
                ShowPreviousRound = roundIndex > 0 ? true : false;

                foreach (var m in otherMatchList.Where(p => p.matchStatus == "IN_PROGRESS"))
			    {
                    var LiveMatchIndex = MatchList.IndexOf(MatchList.Where(p => p.matchId == m.matchId).FirstOrDefault());
                    MatchList[LiveMatchIndex].matchStatus = "IN_PROGRESS";
                    MatchList[LiveMatchIndex].IsLive = true;
                    MatchList[LiveMatchIndex].competitors[0].scoreString = m.homescore.ToString();
                    MatchList[LiveMatchIndex].competitors[1].scoreString = m.awayscore.ToString();
					var TempMatchList = MatchList.ToList();
                    MatchList.ReplaceRange(TempMatchList);
				}
                foreach (var m in otherMatchList.Where(p => p.matchStatus == "COMPLETE"))
                {
                    var LiveMatchIndex = MatchList.IndexOf(MatchList.Where(p => p.matchId == m.matchId).FirstOrDefault());
                    if(MatchList[LiveMatchIndex].matchStatus != "COMPLETE"){
                        MatchList[LiveMatchIndex].competitors[0].scoreString = (m.homescore.ToString());
                        MatchList[LiveMatchIndex].competitors[1].scoreString = m.awayscore.ToString();
                        var TempMatchList = MatchList.ToList();
                        MatchList.ReplaceRange(TempMatchList);
                    }    
                }
			}

            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                MessagingCenter.Send(new MessagingCenterAlert
                {
                    Title = "Error",
                    Message = "Unable to load items.",
                    Cancel = "OK"
                }, "message");
                //ClearCurrents();
            }
            finally
            {
                IsBusy = false;
                IsNotBusy = true;
            }

        }

        string currentRound;
        public string CurrentRound
        {
            get 
            {
                double n;
				if(double.TryParse(Settings.CurrentRoundName, out n)){
                    return AppResources.Round + " " + currentRound;
                } else {
					return Settings.CurrentRoundName;
                }
            }
            set 
            { 
                currentRound = value;
                OnPropertyChanged(nameof(CurrentRound));
            }
        }

        int compSelectedIndex;
        public int CompSelectedIndex
        {
            get
            {
                return compSelectedIndex;
            }
            set
            {
                if (compSelectedIndex != value)
                {
                    compSelectedIndex = value;
                    OnPropertyChanged(nameof(CompSelectedIndex));
                    if (Settings.CurrentComp != CompList[compSelectedIndex].competitionId.ToString())
                    {
                        Settings.CurrentComp = CompList[compSelectedIndex].competitionId.ToString();
                        CurrentCompName = CompList[compSelectedIndex].competitionId.ToString();
						ClearCurrents();
						HasPools = false;
                        HasPhases = false;
                        HasRounds = false;
                        HasMatchTypes = false;
                        MatchList.Clear();
                        SameComp = false;
                        LoadMatchesCommand.Execute(null);
                    }

                } else {
					compSelectedIndex = value;
					OnPropertyChanged(nameof(CompSelectedIndex));
				}
			}
        }

        int dateSelectedIndex;
        public int DateSelectedIndex
        {
            get
            {
                return dateSelectedIndex;
            }
            set
            {
                if (dateSelectedIndex != value)
                {
                    dateSelectedIndex = value;
                    OnPropertyChanged(nameof(DateSelectedIndex));
                    DateTime selectDate = DateTime.Parse(DateList[dateSelectedIndex]);

                    if (Settings.CurrentDate != selectDate.ToString("yyyy-MM-dd"))
                    {
                        Settings.CurrentDate = selectDate.ToString("yyyy-MM-dd");
                        MatchList.Clear();
                        LoadMatchesCommand.Execute(null);
                    }

                }
                else
                {
                    dateSelectedIndex = value;
                    OnPropertyChanged(nameof(DateSelectedIndex));
                }
            }
        }


		public ICommand ChangeRound
        {
            get { return changeRound; }
        }

        void ChangeRoundTap (object s)
        {
            int roundIndex = -1;
            for (int i = 0; i < RoundList.Count; i++)
            {
                if (String.Equals(RoundList[i].name, Settings.CurrentRoundName, StringComparison.OrdinalIgnoreCase))
                {
                    roundIndex = i;
                    break;
                }
            }
            int args = int.Parse(s.ToString());
			Settings.CurrentRound = RoundList[roundIndex + args].number;
			Settings.CurrentRoundName = RoundList[roundIndex + args].name;
            LoadMatchesCommand.Execute(null);
        }

        public ICommand PoolCommand
        {
            get { return poolCommand; }
        }

        void PoolTapped(object s)
        {
            PoolDetail pool = (PoolDetail)s;
			ClearCurrents();
			Settings.CurrentPool = pool.poolId;
            LoadMatchesCommand.Execute(null);
        }

        public ICommand PhaseCommand
        {
            get { return phaseCommand; }
        }

        void PhaseTapped(object s)
        {
            PhaseDetail phase = (PhaseDetail)s;
			ClearCurrents();
			Settings.CurrentPhase = phase.name;
            LoadMatchesCommand.Execute(null);
        }
        public ICommand MatchTypeCommand
        {
            get { return matchTypeCommand; }
        }

        void MatchTypeTapped(object s)
        {
            MatchType matchType = (MatchType)s;
            ClearCurrents();
            Settings.CurrentMatchType = matchType.matchType;
            LoadMatchesCommand.Execute(null);
        }

		void ClearCurrents()
        {
			Settings.CurrentPool = String.Empty;
			Settings.CurrentPhase = String.Empty;
			Settings.CurrentMatchType = String.Empty;
			Settings.CurrentRound = String.Empty;
			Settings.CurrentRoundName = String.Empty;
            Settings.CurrentDate = String.Empty;
		}
	}
}
