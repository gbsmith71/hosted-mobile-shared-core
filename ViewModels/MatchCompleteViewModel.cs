﻿using System;
using System.Resources;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using HostedMobile.Core.ResX;
using System.Windows.Input;
using System.Linq;
using Xamarin.Forms;

namespace HostedMobile
{
    public class MatchCompleteViewModel : BaseViewModel
    {
		public Command LoadMatchSummaryCommand { get; set; }
		public Command LoadBoxScoreCommand { get; set; }
		public Command LoadPlaysCommand { get; set; }

		public ObservableRangeCollection<PeriodScore> HomePeriodScores { get; set; }
		public ObservableRangeCollection<PeriodScore> AwayPeriodScores { get; set; }
		public ObservableRangeCollection<LeadersBlockContent> LeadersData { get; set; }
		public List<LeadersBlockContent> LeadersDataList { get; set; }
		public Match _matchDetails { get; set; }
		public ObservableRangeCollection<StatBar> ComparisonStats { get; set; }

        public ObservableRangeCollection<Competitor> BoxscoreCompetitors { get; set; }
        public ObservableRangeCollection<Boxscore> BoxscoreDetails { get; set; }
		public ObservableRangeCollection<Boxscore> BoxscoreTeamDetails { get; set; }

        public ObservableRangeCollection<Playbyplaydata> PlayByPlayData { get; set; }

		public ObservableRangeCollection<PeriodButton> PeriodButtons { get; set; }
		public ObservableRangeCollection<PeriodButton> TempPeriodButtons { get; set; }

		public MatchBoxscore matchBoxscore;
        public PlayByPlay matchPlayByPlay;

		ICommand teamCommand;
		ICommand periodCommand;

		public MatchCompleteViewModel(Match matchDetails)
        {
			Title = AppResources.ResourceManager.GetString("MatchSummary");
			_matchDetails = matchDetails;
			HomePeriodScores = new ObservableRangeCollection<PeriodScore>();
			AwayPeriodScores = new ObservableRangeCollection<PeriodScore>();
			ComparisonStats = new ObservableRangeCollection<StatBar>();
			LeadersData = new ObservableRangeCollection<LeadersBlockContent>();
            BoxscoreCompetitors = new ObservableRangeCollection<Competitor>();
            BoxscoreDetails = new ObservableRangeCollection<Boxscore>();
            BoxscoreTeamDetails = new ObservableRangeCollection<Boxscore>();
            PlayByPlayData = new ObservableRangeCollection<Playbyplaydata>();
            PeriodButtons = new ObservableRangeCollection<PeriodButton>();
			TempPeriodButtons = new ObservableRangeCollection<PeriodButton>();
            LeadersDataList = new List<LeadersBlockContent>();
			IsNotBusy = false;
            inOT = false;

			LoadMatchSummaryCommand = new Command(async () => await ExecuteLoadMatchSummaryCommand(_matchDetails));
			LoadBoxScoreCommand = new Command(async () => await ExecuteLoadBoxScoreCommand(_matchDetails));
            LoadPlaysCommand = new Command(async () => await ExecuteLoadPlaysCommand(_matchDetails));
			teamCommand = new Command(TeamTapped);
			periodCommand = new Command(PeriodTapped);

            PageButtons.Add(new PageButton() { ButtonText = AppResources.Summary, Current = true, IndexTab = 0 });
            PageButtons.Add(new PageButton() { ButtonText = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_BoxScore"), Current = false, IndexTab = 1 });
            PageButtons.Add(new PageButton() { ButtonText = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_PlayByPlay"), Current = false, IndexTab = 2 });
			PagePosition = 2;
			PagePosition = 0;

		}
		async Task ExecuteLoadMatchSummaryCommand(Match matchDetails)
		{
			if (IsBusy)
				return;

			IsBusy = true;
			IsNotBusy = false;
            try
            {
                var hs = new HostedDataService();

                App.currentMatch = matchDetails;

                var matchSummary = await hs.GetMatchSummary(matchDetails.matchId.ToString(), true);
                if (matchSummary.periodbyperiod != null){
                    if (matchSummary.periodbyperiod.homeperiods.Count > (matchSummary.common.CompDetails.numberOfPeriods + 1))
                    {
                        inOT = true;
                    }
                    HomePeriodScores.Clear();
                    HomePeriodScores.ReplaceRange(matchSummary.periodbyperiod.homeperiods);
                    AwayPeriodScores.Clear();
                    AwayPeriodScores.ReplaceRange(matchSummary.periodbyperiod.awayperiods);
                }

                LeadersData.Clear();
                for (var i = 0; i < matchSummary.leadersFieldOrder.Count; i++)
                {
                    var block = new LeadersBlockContent();
                    block.Title = AppResources.ResourceManager.GetString("STAT_PERSONCOMP_" + GlobalVariables.Sport + "_" + matchSummary.leadersFieldOrder[i] + "_NAME");
                    var firstFive = matchSummary.leaderData[i].Take(5).ToList();
                    block.Leaders = firstFive;
                    if (block.Leaders.Count() > 0)
                    {
                        LeadersData.Add(block);
                    }
                }
                LeadersDataList.Clear();
                LeadersDataList.AddRange(LeadersData.ToList());

                if (LeadersData.Count > 1)
                {
                    Position = 1;
                    await DelayCarousel(500);
                }

                ComparisonStats.Clear();
				foreach (var currentItem in matchSummary.compareFieldOrder)
				{
					var newLine = new StatBar();
					newLine.title = AppResources.ResourceManager.GetString("STAT_TEAMMATCH_" + GlobalVariables.Sport + "_" + currentItem.ToString() + "_NAME");
					newLine.IsLiveMatch = false;
					newLine.IsPercent = false;
                    newLine.team1 = Convert.ToDouble(matchSummary.comparisonStats[0].GetType().GetRuntimeProperty(currentItem.ToString()).GetValue(matchSummary.comparisonStats[0], null));
                    newLine.team2 = Convert.ToDouble(matchSummary.comparisonStats[1].GetType().GetRuntimeProperty(currentItem.ToString()).GetValue(matchSummary.comparisonStats[1], null));
					if (currentItem.Contains("Percent"))
					{
						newLine.IsPercent = true;
					}
					ComparisonStats.Add(newLine);
				}

                PageButtons.Clear();
                PageButtons.Add(new PageButton() { ButtonText = AppResources.Summary, Current = true, Page = "summary" });
                PageButtons.Add(new PageButton() { ButtonText = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_BoxScore"), Current = false, Page = "boxscore" });
                PageButtons.Add(new PageButton() { ButtonText = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_PlayByPlay"), Current = false, Page = "plays" });
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);
				MessagingCenter.Send(new MessagingCenterAlert
				{
					Title = "Error",
					Message = "Unable to load items.",
					Cancel = "OK"
				}, "message");
			}
			finally
			{
				IsBusy = false;
				IsNotBusy = true;
                await ExecuteLoadBoxScoreCommand(_matchDetails);
			}

		}

		async Task ExecuteLoadBoxScoreCommand(Match matchDetails)
		{
			if (IsBusy)
				return;

			//IsBusy = true;
			//IsNotBusy = false;

			try
			{
				var hs = new HostedDataService();
                matchBoxscore = await hs.GetMatchBoxscore(matchDetails.matchId.ToString(), true);

				for (var i = 0; i < matchBoxscore.boxscore_hometeam.Count; i++)
				{
					matchBoxscore.boxscore_hometeam[i].index = i + 1;
				}
                for (var i = 0; i < matchBoxscore.boxscore_awayteam.Count; i++)
				{
					matchBoxscore.boxscore_awayteam[i].index = i + 1;
				}

				BoxscoreCompetitors.Clear();
                matchBoxscore.matchDetail.competitors[0].current = true;
                BoxscoreCompetitors.ReplaceRange(matchBoxscore.matchDetail.competitors);

                BoxscoreDetails.Clear();
                BoxscoreDetails.ReplaceRange(matchBoxscore.boxscore_hometeam);

				BoxscoreTeamDetails.Clear();
                BoxscoreTeamDetails.ReplaceRange(matchBoxscore.boxscoreteam_hometeam);
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);
				MessagingCenter.Send(new MessagingCenterAlert
				{
					Title = "Error",
					Message = "Unable to load items.",
					Cancel = "OK"
				}, "message");
			}
			finally
			{
				IsBusy = false;
				IsNotBusy = true;
				await ExecuteLoadPlaysCommand(_matchDetails);

			}

		}

		async Task ExecuteLoadPlaysCommand(Match matchDetails)
		{
			if (IsBusy)
				return;

			//IsBusy = true;
			//IsNotBusy = false;

			try
			{
				var hs = new HostedDataService();
                matchPlayByPlay = await hs.GetMatchPlays(matchDetails.matchId.ToString(), true);

                matchPlayByPlay.playbyplaydata.RemoveAll(item => item.actionType == "clock");

                matchPlayByPlay.playbyplaydata.Sort((x, y) => x.actionNumber.CompareTo(y.actionNumber));
                matchPlayByPlay.playbyplaydata.Reverse();
                foreach (var play in matchPlayByPlay.playbyplaydata.Where(x => x.teamId == matchDetails.competitors[0].teamId)) {
					play.isHome = true;
                    play.isAway = false;
					play.teamCode = matchDetails.competitors[0].teamCode;
                    if (matchDetails.competitors[0].images != null)
                    {
                        play.teamLogo = matchDetails.competitors[0].images.logo.T1.url;
                    } else {
                        play.teamLogo = "";
                    }
                }
				foreach (var play in matchPlayByPlay.playbyplaydata.Where(x => x.teamId == matchDetails.competitors[1].teamId))
				{
                    play.isHome = false;
                    play.isAway = true;
                    play.teamCode = matchDetails.competitors[1].teamCode;
                    if (matchDetails.competitors[1].images != null)
                    {
                        play.teamLogo = matchDetails.competitors[1].images.logo.T1.url;
                    }
                    else
                    {
                        play.teamLogo = "";
                    }
				}
				PeriodButtons.Clear();
				TempPeriodButtons.Clear();
				PlayByPlayData.Clear();

				string[] hiddenActions = { "pass", "" };

                var P1Button = new PeriodButton();
                P1Button.periodData = matchPlayByPlay.playbyplaydata.Where(item => (item.periodType == "REGULAR") && (item.period == 1) && (hiddenActions.Contains(item.actionType) == false)).ToList();
				P1Button.title = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_Period1");
				if (P1Button.periodData.Count > 0){
                    P1Button.IsVisible = true;
                    P1Button.current = false;
				}
                TempPeriodButtons.Add(P1Button);

                var P2Button = new PeriodButton();
				P2Button.periodData = matchPlayByPlay.playbyplaydata.Where(item => (item.periodType == "REGULAR") && (item.period == 2) && (hiddenActions.Contains(item.actionType) == false)).ToList();
				P2Button.title = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_Period2");
				if (P2Button.periodData.Count > 0)
				{
					P2Button.IsVisible = true;
                    P2Button.current = false;
				}
				TempPeriodButtons.Add(P2Button);

				var P3Button = new PeriodButton();
				P3Button.periodData = matchPlayByPlay.playbyplaydata.Where(item => (item.periodType == "REGULAR") && (item.period == 3) && (hiddenActions.Contains(item.actionType) == false)).ToList();
				P3Button.title = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_Period3");
				if (P3Button.periodData.Count > 0)
				{
					P3Button.IsVisible = true;
                    P3Button.current = false;
				}
				TempPeriodButtons.Add(P3Button);

				var P4Button = new PeriodButton();
				P4Button.periodData = matchPlayByPlay.playbyplaydata.Where(item => (item.periodType == "REGULAR") && (item.period == 4) && (hiddenActions.Contains(item.actionType) == false)).ToList();
				P4Button.title = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_Period4");
				if (P4Button.periodData.Count > 0)
				{
					P4Button.IsVisible = true;
					P4Button.current = false;
				}
				TempPeriodButtons.Add(P4Button);

				var OTButton = new PeriodButton();
                OTButton.periodData = matchPlayByPlay.playbyplaydata.Where(item => (item.periodType == "OVERTIME") && (hiddenActions.Contains(item.actionType) == false)).ToList();
				OTButton.title = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_OVERTIME_ABBREV");
				if (OTButton.periodData.Count > 0)
				{
					OTButton.IsVisible = true;
					OTButton.current = false;
				}
				TempPeriodButtons.Add(OTButton);

                var ETButton = new PeriodButton();
                ETButton.periodData = matchPlayByPlay.playbyplaydata.Where(item => (item.periodType == "EXTRATIME") && (hiddenActions.Contains(item.actionType) == false)).ToList();
                ETButton.title = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_OVERTIME_ABBREV");
                if (ETButton.periodData.Count > 0)
                {
                    ETButton.IsVisible = true;
                    ETButton.current = false;
                }
                TempPeriodButtons.Add(ETButton);

                var SOButton = new PeriodButton();
                SOButton.periodData = matchPlayByPlay.playbyplaydata.Where(item => (item.periodType == "PENALTYSHOOTOUT") && (hiddenActions.Contains(item.actionType) == false)).ToList();
                SOButton.title = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_SHOOTOUT_ABBREV");
                if (SOButton.periodData.Count > 0)
                {
                    SOButton.IsVisible = true;
                    SOButton.current = false;
                }
                TempPeriodButtons.Add(SOButton);

                if (OTButton.IsVisible == true)
                {
                    PlayByPlayData.ReplaceRange(OTButton.periodData);
                    OTButton.current = true;
                } else if (SOButton.IsVisible){
                    PlayByPlayData.ReplaceRange(SOButton.periodData);
                    SOButton.current = true;
                } else if (ETButton.IsVisible){
                    PlayByPlayData.ReplaceRange(ETButton.periodData);
                    ETButton.current = true;
                } else if (P4Button.IsVisible){
					PlayByPlayData.ReplaceRange(P4Button.periodData);
					P4Button.current = true;
                } else {
					PlayByPlayData.ReplaceRange(P2Button.periodData);
					P2Button.current = true;
				}

                PeriodButtons.ReplaceRange(TempPeriodButtons);
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);
				MessagingCenter.Send(new MessagingCenterAlert
				{
					Title = "Error",
					Message = "Unable to load items.",
					Cancel = "OK"
				}, "message");
			}
			finally
			{
				IsBusy = false;
				IsNotBusy = true;
			}

		}

        public ICommand TeamCommand
		{
			get { return teamCommand; }
		}

        void TeamTapped(object s)
        {
			BoxscoreCompetitors.Clear();
			BoxscoreDetails.Clear();
			BoxscoreTeamDetails.Clear();
			var team = (Competitor)s;
            if (team.isHomeCompetitor == 1)
            {
				matchBoxscore.matchDetail.competitors[0].current = true;
                matchBoxscore.matchDetail.competitors[1].current = false;
				BoxscoreCompetitors.ReplaceRange(matchBoxscore.matchDetail.competitors);
				BoxscoreDetails.ReplaceRange(matchBoxscore.boxscore_hometeam);
				BoxscoreTeamDetails.ReplaceRange(matchBoxscore.boxscoreteam_hometeam);

			} else {
				matchBoxscore.matchDetail.competitors[0].current = false;
				matchBoxscore.matchDetail.competitors[1].current = true;
				BoxscoreCompetitors.ReplaceRange(matchBoxscore.matchDetail.competitors);
				BoxscoreDetails.ReplaceRange(matchBoxscore.boxscore_awayteam);
				BoxscoreTeamDetails.ReplaceRange(matchBoxscore.boxscoreteam_awayteam);
			}

		}

		public ICommand PeriodCommand
		{
			get { return periodCommand; }
		}

		void PeriodTapped(object s)
		{
			IsBusy = true;
            IsNotBusy = false;

			PlayByPlayData.Clear();
            PeriodButtons.Select(c => { c.current = false; return c; }).ToList();
            var period = (PeriodButton)s;
            foreach (var p in PeriodButtons.Where(w => w.title == period.title))
			{
                p.current = true;
			}

            TempPeriodButtons.Clear();
            TempPeriodButtons.ReplaceRange(PeriodButtons);
            PeriodButtons.Clear();
            PeriodButtons.ReplaceRange(TempPeriodButtons.ToList());
            PlayByPlayData.ReplaceRange(period.periodData);
			
            IsBusy = false;
			IsNotBusy = true;

		}
	}
}