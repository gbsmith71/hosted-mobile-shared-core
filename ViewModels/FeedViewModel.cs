﻿using System;
using System.Resources;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using HostedMobile.Core.ResX;
using System.Linq;
using System.Reactive.Linq;
using Plugin.Connectivity;
using System.Net.Http;
using System.Xml.Linq;
using System.Net;
using Akavache;

using Xamarin.Forms;

namespace HostedMobile
{
    public class FeedViewModel : BaseViewModel
    {
        public Command LoadNewsCommand { get; set; }
        public Command LoadVideosCommand { get; set; }

        public ObservableRangeCollection<RSSFeedItem> FeedItems { get; set; }
        public ObservableRangeCollection<RSSFeedItem> VideoFeedItems { get; set; }

        ICommand newsCommand;


        public FeedViewModel()
        {
            Title = AppResources.ResourceManager.GetString("LatestNews");

            FeedItems = new ObservableRangeCollection<RSSFeedItem>();
            VideoFeedItems = new ObservableRangeCollection<RSSFeedItem>();

            LoadNewsCommand = new Command(async () => await ExecuteLoadNewsCommand());
            newsCommand = new Command(NewsTapped);
            LoadVideosCommand = new Command(async () => await ExecuteLoadVideosCommand());

            PageButtons.Add(new PageButton() { ButtonText = AppResources.News, Current = true, IndexTab = 0 });
            PageButtons.Add(new PageButton() { ButtonText = AppResources.Videos, Current = false, IndexTab = 1 });
        }

        async Task ExecuteLoadNewsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            IsNotBusy = false;

            try
            {

                var feed = GlobalVariables.RSSFeedURL;
                var responseString = await GetFeed(feed, "newsFeed");                

                FeedItems.Clear();
                var items = await ParseFeed(responseString);
                foreach (var item in items)
                {
                    FeedItems.Add(item);
                }

            }
            catch (Exception ex)
            {                Debug.WriteLine(ex);
                MessagingCenter.Send(new MessagingCenterAlert
                {
                    Title = "Error",
                    Message = "Unable to load items.",
                    Cancel = "OK"
                }, "message");
            }
            finally
            {
                IsBusy = false;
                IsNotBusy = true;
                await ExecuteLoadVideosCommand();
            }

        }

        async Task ExecuteLoadVideosCommand()
        {
            if (IsBusy)
                return;

            //IsBusy = true;
            //IsNotBusy = false;

            try
            {

                var ytFeed = "https://www.youtube.com/feeds/videos.xml?channel_id=" + GlobalVariables.YouTubeChannelID;
                var ytResponseString = await GetFeed(ytFeed, "ytFeed");

                VideoFeedItems.Clear();
                var items = await ParseYTFeed(ytResponseString);
                foreach (var item in items)
                {
                    VideoFeedItems.Add(item);
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                MessagingCenter.Send(new MessagingCenterAlert
                {
                    Title = "Error",
                    Message = "Unable to load items.",
                    Cancel = "OK"
                }, "message");
            }
            finally
            {
                IsBusy = false;
                IsNotBusy = true;
            }

        }

        public ICommand NewsCommand
        {
            get { return newsCommand; }
        }

        void NewsTapped(object s)
        {
            var news = (RSSFeedItem)s;
        }

        public async Task<String> GetFeed(string rssFeed, string cacheKey)
        {
            return await BlobCache.UserAccount.GetOrFetchObject<String>("rssFeed_" + cacheKey,
                async () => await ReturnFeed(rssFeed),
                DateTimeOffset.Now.AddMinutes(120));
        }

        public async Task<String> ReturnFeed(string rssFeed)
        {
            String feedContent = "";

            if (CrossConnectivity.Current.IsConnected)
            {
                var httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
                feedContent = await httpClient.GetStringAsync(rssFeed);
            }

            return feedContent;
        }


        private async Task<List<RSSFeedItem>> ParseFeed(string rss)
        {
            XNamespace content = "http://purl.org/rss/1.0/modules/content/";

            if (GlobalVariables.ImageType == "element")
            {
                return await Task.Run(() =>
                {
                    var xdoc = XDocument.Parse(rss);
                    var id = 0;

                    return (from item in xdoc.Descendants("item")
                            select new RSSFeedItem
                            {
                                Title = (string)item.Element("title"),
                                Description = (string)item.Element("description"),
                                Content = GlobalVariables.ContentAvailable != false ? item.Element(content.GetName("encoded")).Value : (string)item.Element("description"),
                                Link = (string)item.Element("link"),
                                PublishDate = (string)item.Element("pubDate"),
                                Image = item.Element(GlobalVariables.ImageElement1) != null ? (string)item.Element(GlobalVariables.ImageElement1) : null,
                                Id = id++
                            }).ToList();
                });
            } else if (GlobalVariables.ImageType == "sub-element")
                {
                    return await Task.Run(() =>
                    {
                        var xdoc = XDocument.Parse(rss);
                        var id = 0;

                        return (from item in xdoc.Descendants("item")
                                select new RSSFeedItem
                                {
                                    Title = (string)item.Element("title"),
                                    Description = (string)item.Element("description"),
                                    Content = GlobalVariables.ContentAvailable != false ? item.Element(content.GetName("encoded")).Value : (string)item.Element("description"),
                                    Link = (string)item.Element("link"),
                                    PublishDate = (string)item.Element("pubDate"),
                                    Image = item.Element(GlobalVariables.ImageElement1).Element(GlobalVariables.ImageElement2) != null ? (string)item.Element(GlobalVariables.ImageElement1).Element(GlobalVariables.ImageElement2) : null,
                                    Id = id++
                                }).ToList();
                    });
            } else if (GlobalVariables.ImageType == "media") {
                return await Task.Run(() =>
                {
                    var xdoc = XDocument.Parse(rss);
                    var id = 0;

                    return (from item in xdoc.Descendants("item")
                            select new RSSFeedItem
                            {
                                Title = (string)item.Element("title"),
                                Description = (string)item.Element("description"),
                                Content = GlobalVariables.ContentAvailable != false ? item.Element(content.GetName("encoded")).Value : (string)item.Element("description"),
                                Link = (string)item.Element("link"),
                                PublishDate = (string)item.Element("pubDate"),
                                Image = item.Element(GlobalVariables.mediaNamespace + GlobalVariables.ImageElement1) != null ? (string)item.Element(GlobalVariables.mediaNamespace + GlobalVariables.ImageElement1).Attribute(GlobalVariables.ImageElement2).Value : null,
                                Id = id++
                            }).ToList();
                });
            } else {
                return await Task.Run(() =>
                {
                    var xdoc = XDocument.Parse(rss);
                    var id = 0;

                    return (from item in xdoc.Descendants("item")
                            select new RSSFeedItem
                            {
                                Title = (string)item.Element("title"),
                                Description = (string)item.Element("description"),
                                Content = GlobalVariables.ContentAvailable != false ? item.Element(content.GetName("encoded")).Value : (string)item.Element("description"),
                                Link = (string)item.Element("link"),
                                PublishDate = (string)item.Element("pubDate"),
                                Image = item.Element(GlobalVariables.ImageElement1) != null ? (string)item.Element(GlobalVariables.ImageElement1).Attribute(GlobalVariables.ImageElement2).Value : null,
                                Id = id++
                            }).ToList();
                });
            }
        }
        private async Task<List<RSSFeedItem>> ParseYTFeed(string ytRss)
        {
            XNamespace ns = "http://www.w3.org/2005/Atom";
            XNamespace media = "http://search.yahoo.com/mrss/";
            XNamespace yt = "http://www.youtube.com/xml/schemas/2015";

            return await Task.Run(() =>
            {
                var xdoc = XDocument.Parse(ytRss);
                var id = 0;

                return (from item in xdoc.Descendants(ns+"entry")
                        select new RSSFeedItem
                        {
                            Title = (string)item.Element(ns+"title"),
                            Description = (string)item.Element(media + "group").Element(media + "description"),
                            Link = (string)item.Element(yt +"videoId"),
                            PublishDate = (string)item.Element(ns+"published"),
                            Image = (string)item.Element(media + "group").Element(media + "thumbnail").Attribute(("url")),
                            Id = id++
                        }).ToList();
            });
        }
    }
}