﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using HostedMobile.Core.ResX;
using Xamarin.Forms;

namespace HostedMobile
{
    public class TeamRosterViewModel : BaseViewModel
    {
		public ObservableRangeCollection<PlayerData> Players { get; set; }
		public Command LoadRosterCommand { get; set; }
		public String[] FieldList;
        ICommand tapCommand;

        public TeamRosterViewModel(Team teamObject)
        {
			Title = AppResources.ResourceManager.GetString("Roster");
			Players = new ObservableRangeCollection<PlayerData>();
            LoadRosterCommand = new Command(async () => await ExecuteLoadRosterCommand(teamObject.teamId.ToString())); 
            tapCommand = new Command(OnTapped);
        }

		void OnTapped(object s)
		{
			Debug.WriteLine("parameter: " + s);
		}

        async Task ExecuteLoadRosterCommand(String teamId)
		{
			if (IsBusy)
				return;

			IsBusy = true;
            IsNotBusy = false;
			try
			{
				var hs = new HostedDataService();
				Players.Clear();
				var playerList = await hs.GetRoster(teamId, true);
				FieldList = playerList.fieldOrder.ToArray();
				TeamName = playerList.teamDetail.teamName;
				TeamLogo = playerList.teamDetail.images.logo.T1.url;
				for (var i = 0; i < playerList.playerData.Count; i++)
				{
					playerList.playerData[i].index = i + 1;
				}
				var orderedList = playerList.playerData;
				Players.ReplaceRange(orderedList);
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);
				MessagingCenter.Send(new MessagingCenterAlert
				{
					Title = "Error",
					Message = "Unable to load items.",
					Cancel = "OK"
				}, "message");
			}
			finally
			{
				IsBusy = false;
				IsNotBusy = true;
			}

		}

		string teamName = string.Empty;
		public string TeamName
		{
			get { return teamName; }
			set { SetProperty(ref teamName, value); }
		}

        string teamLogo = string.Empty;
		public string TeamLogo
		{
			get { return teamLogo; }
			set { SetProperty(ref teamLogo, value); }
		}

        public ICommand TapCommand
		{
			get { return tapCommand; }
		}
    }
}
