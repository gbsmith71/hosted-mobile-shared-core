﻿using System;
using System.Resources;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using HostedMobile.Core.ResX;
using System.Windows.Input;
using System.Linq;
using Xamarin.Forms;

namespace HostedMobile
{
    public class MatchScheduledViewModel : BaseViewModel
    {
		public Command LoadMatchPreviewCommand { get; set; }
        public ObservableRangeCollection<Head2head> HeadToHead { get; set; }
        public ObservableRangeCollection<LeadersBlockContent> LeadersData { get; set; }
		public List<LeadersBlockContent> LeadersDataList { get; set; }
		public String LeadersDataTitle { get; set; }
		public Match _matchDetails { get; set; }
		public ObservableRangeCollection<StatBar> ComparisonStats { get; set; }


		public MatchScheduledViewModel(Match matchDetails)
        {
			Title = AppResources.ResourceManager.GetString("MatchPreview");
			_matchDetails = matchDetails;
            HeadToHead = new ObservableRangeCollection<Head2head>();
            ComparisonStats = new ObservableRangeCollection<StatBar>();

            LeadersData = new ObservableRangeCollection<LeadersBlockContent>();
			LeadersDataList = new List<LeadersBlockContent>();
			LeadersDataTitle = "";
			IsNotBusy = false;

			LoadMatchPreviewCommand = new Command(async () => await ExecuteLoadMatchPreviewCommand(_matchDetails));
		}

		async Task ExecuteLoadMatchPreviewCommand(Match matchDetails)
		{
			if (IsBusy)
				return;

			IsBusy = true;
			IsNotBusy = false;

			try
			{
				var hs = new HostedDataService();
                var matchPreview = await hs.GetMatchPreview(matchDetails.matchId.ToString(), true);

                HasHeadToHead = false;
                if (matchPreview.head2head != null)
                {
                    if (matchPreview.head2head.Count() > 0)
                    {
                        for (var i = 0; i < matchPreview.head2head.Count(); i++)
                        {
                            matchPreview.head2head[i].index = i + 1;
                        }
                        HeadToHead.Clear();
                        HeadToHead.ReplaceRange(matchPreview.head2head);
                        HasHeadToHead = true;
                    }
                }
                LeadersData.Clear();
                for (var i = 0; i < matchPreview.leadersFieldOrder.Count; i++)
                {
                    var block = new LeadersBlockContent();
                    block.Title = AppResources.ResourceManager.GetString("STAT_PERSONCOMP_" + GlobalVariables.Sport + "_" + matchPreview.leadersFieldOrder[i] + "_NAME");
                    block.Leaders = matchPreview.leaderData[i];
                    if (block.Leaders.Count() > 0)
                    {
                        LeadersData.Add(block);
                    }
                }
                LeadersDataList.Clear();
                LeadersDataList.AddRange(LeadersData.ToList());

                if (matchPreview.leadersFieldOrder.Count > 1)
                {
                    Position = 1;
                    await DelayCarousel(500);
                }

				ComparisonStats.Clear();
				foreach (var currentItem in matchPreview.compareFieldOrder)
				{
                    var newLine = new StatBar();
                    newLine.title = AppResources.ResourceManager.GetString("STAT_TEAMMATCH_" + GlobalVariables.Sport + "_" + currentItem.ToString() + "_NAME");
                    newLine.IsLiveMatch = false;
					newLine.IsPercent = false;
					if (currentItem.Contains("Percent"))
					{
						newLine.IsPercent = true;
					}
					newLine.team1 = Convert.ToDouble(matchPreview.comparisonStats[0].GetType().GetRuntimeProperty(currentItem.ToString()).GetValue(matchPreview.comparisonStats[0], null));
					newLine.team2 = Convert.ToDouble(matchPreview.comparisonStats[1].GetType().GetRuntimeProperty(currentItem.ToString()).GetValue(matchPreview.comparisonStats[1], null));
                    ComparisonStats.Add(newLine);
				}

			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);
				MessagingCenter.Send(new MessagingCenterAlert
				{
					Title = "Error",
					Message = "Unable to load items.",
					Cancel = "OK"
				}, "message");
			}
			finally
			{
				IsBusy = false;
				IsNotBusy = true;
			}

		}

	}
}

