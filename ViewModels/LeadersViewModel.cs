﻿using System;
using System.Resources;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using HostedMobile.Core.ResX;
using System.Windows.Input;
using System.Linq;
using System.Reactive.Linq;

using Xamarin.Forms;

namespace HostedMobile
{
    public class LeadersViewModel : BaseViewModel
    {
        public Command LoadLeadersCommand { get; set; }
        public ObservableRangeCollection<Leaders> LeadersList { get; set; }
        public ObservableRangeCollection<String> Comps { get; set; }
        public int CurrentComp { get; set; }
        public string CurrentCompName { get; set; }
        public List<Competition> CompList;
        public List<String> compList;
        public String[] FieldList;
        public ObservableRangeCollection<LeadersBlockContent> LeadersData { get; set; }
        public ObservableRangeCollection<LeadersBlockContent> LeadersDataList { get; set; }

        public LeadersViewModel()
        {
            Title = AppResources.ResourceManager.GetString("Leaders");
            Comps = new ObservableRangeCollection<String>();
            LoadLeadersCommand = new Command(async () => await ExecuteLoadLeadersCommand());
            LeadersData = new ObservableRangeCollection<LeadersBlockContent>();
            LeadersDataList = new ObservableRangeCollection<LeadersBlockContent>();
            IsNotBusy = false;
            inOT = false;

        }

        async Task ExecuteLoadLeadersCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            IsNotBusy = false;

            try
            {
                var hs = new HostedDataService();
                var compLeadersList = await hs.GetCompLeaders(true);

                CompList = compLeadersList.FilterLists.competitions;
                FieldList = compLeadersList.fields.ToArray();
                var leaderPacks = compLeadersList.leaders;

                LeadersData.Clear();
                foreach (var statistic in FieldList)
                {
                    object leaderBlock = leaderPacks.GetType().GetRuntimeProperty(statistic).GetValue(leaderPacks);
                    List<LeaderDetails> leadersBlock = (List<LeaderDetails>)leaderBlock;
                    foreach (var leader in leadersBlock){
                        if (statistic.IndexOf("Percent") > 0 && Double.Parse(leader.value) > 0){
                            Double leaderValue =  (Double.Parse(leader.value)) * 100;
                            leader.value = leaderValue.ToString() + "%";
                        }
                        if (statistic.IndexOf("Minute") > 0 && Double.Parse(leader.value) > 0)
                        {
                            leader.value = leader.value.Replace(".",":");
                        }
                    }
                    leadersBlock.RemoveAll(item => item.value == "0");
                    var block = new LeadersBlockContent();
                    block.Title = AppResources.ResourceManager.GetString("STAT_PERSONCOMP_" + GlobalVariables.Sport + "_" + statistic + "_NAME");
                    block.Leaders = leadersBlock.Take(10).ToList();;
                    if (block.Leaders.Count() > 0)
                    {
                        LeadersData.Add(block);
                    }
                }
                LeadersDataList.ReplaceRange(LeadersData.ToList());

                if (LeadersDataList.Count > 1)
                {
                    Position = LeadersDataList.Count - 1;
                    await DelayCarousel(500);
                }

                //Load Comp Picker
                if (Comps.Count == 0)
                {
                    Comps.Clear();
                    compList = hs.GetFilterComps(compLeadersList.FilterLists);
                    Comps.ReplaceRange(compList);
                }
                CompSelectedIndex = hs.GetFilterCompsSelected(compLeadersList.FilterLists, compLeadersList.competitionId);


            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                MessagingCenter.Send(new MessagingCenterAlert
                {
                    Title = "Error",
                    Message = "Unable to load items.",
                    Cancel = "OK"
                }, "message");
            }
            finally
            {
                IsBusy = false;
                IsNotBusy = true;
            }

        }

        int compSelectedIndex;
        public int CompSelectedIndex
        {
            get
            {
                return compSelectedIndex;
            }
            set
            {
                if (compSelectedIndex != value)
                {
                    compSelectedIndex = value;
                    OnPropertyChanged(nameof(CompSelectedIndex));
                    if (Settings.CurrentComp != CompList[compSelectedIndex].competitionId.ToString())
                    {
                        Settings.CurrentComp = CompList[compSelectedIndex].competitionId.ToString();
                        CurrentCompName = CompList[compSelectedIndex].competitionId.ToString();
                        ClearCurrents();
                        //LeadersList.Clear();
                        SameComp = false;
                        LoadLeadersCommand.Execute(null);
                    }
                }
                else
                {
                    compSelectedIndex = value;
                    OnPropertyChanged(nameof(CompSelectedIndex));
                }
            }
        }
        void ClearCurrents()
        {
            Settings.CurrentPool = String.Empty;
            Settings.CurrentPhase = String.Empty;
            Settings.CurrentMatchType = String.Empty;
            Settings.CurrentDate = String.Empty;
            Settings.CurrentRound = String.Empty;
        }

    }
}
