﻿using System;
using System.Resources;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using HostedMobile.Core.ResX;
using System.Windows.Input;
using System.Linq;
using Xamarin.Forms;

namespace HostedMobile
{
	public class MatchLiveViewModel : BaseViewModel
	{
		public Command LoadMatchLiveCommand { get; set; }

		public ObservableRangeCollection<PeriodScore> HomePeriodScores { get; set; }
		public ObservableRangeCollection<PeriodScore> AwayPeriodScores { get; set; }
		public ObservableRangeCollection<LiveLeadersBlockContent> LeadersData { get; set; }
		public List<LiveLeadersBlockContent> LeadersDataList { get; set; }
		public ObservableRangeCollection<StatBar> ComparisonStats { get; set; }
		public Match _matchDetails { get; set; }
		public String clock { get; set; }
        public ObservableRangeCollection<Scorer> HomeScorers { get; set; }
        public ObservableRangeCollection<Scorer> AwayScorers { get; set; }

		public MatchLiveViewModel(Match matchDetails)
		{
			Title =  AppResources.ResourceManager.GetString("MatchLive");
			_matchDetails = matchDetails;
			HomePeriodScores = new ObservableRangeCollection<PeriodScore>();
			AwayPeriodScores = new ObservableRangeCollection<PeriodScore>();
			ComparisonStats = new ObservableRangeCollection<StatBar>();
			LeadersData = new ObservableRangeCollection<LiveLeadersBlockContent>();
			LeadersDataList = new List<LiveLeadersBlockContent>();
            HomeScorers = new ObservableRangeCollection<Scorer>(); 
            AwayScorers = new ObservableRangeCollection<Scorer>(); 
			IsNotBusy = false;
			inOT = false;
            isFOOTBALL = false;
            isBASKETBALL = false;


			LoadMatchLiveCommand = new Command(async () => await ExecuteLoadMatchLiveCommand(_matchDetails));

		}
		async Task ExecuteLoadMatchLiveCommand(Match matchDetails)
		{
			if (IsBusy)
				return;

			IsBusy = true;
			IsNotBusy = false;

			try
			{
				var hs = new HostedDataService();
                var matchLive = await hs.GetLiveMatch(matchDetails.matchId.ToString());
                LiveCompetitor homeTeam;
				LiveCompetitor awayTeam;

                if (GlobalVariables.Sport == "FOOTBALL"){
                    isFOOTBALL = true;
                }
                if (GlobalVariables.Sport == "BASKETBALL")
                {
                    isBASKETBALL = true;
                }

                if (matchLive.periodType == "PENALTYSHOOTOUT")
                {
                    int[] tnoP = new int[] { 0, 0 }; ;

                    foreach (var m in matchLive.pbp.Where(p => p.actionType == "shootoutkick"))
                    {
                        if (m.success == 1)
                        {
                            int tno = m.tno - 1;
                            tnoP[tno] = tnoP[tno] + 1;
                        }
                    }
                    HomeScoreSecondaryString = "(" + tnoP[0].ToString() + ")";
                    AwayScoreSecondaryString = "(" + tnoP[1].ToString() + ")";
                }


				if (matchLive.tm.a1.code == _matchDetails.competitors[0].teamCode){
                    if (isFOOTBALL == true && matchLive.scorers != null)
                    {
                        if (matchLive.scorers.Team1 != null)
                        {
                            HomeScorers.ReplaceRange(matchLive.scorers.Team1);
                        }
                        if (matchLive.scorers.Team2 != null)
                        {
                            AwayScorers.ReplaceRange(matchLive.scorers.Team2);
                        }
                    }
                    homeTeam = matchLive.tm.a1;
					awayTeam = matchLive.tm.b2;
                    HomeScoreString = matchLive.tm.a1.score.ToString();
                    AwayScoreString = matchLive.tm.b2.score.ToString();
				} else {
                    if (isFOOTBALL == true && matchLive.scorers != null)
                    {
                        if (matchLive.scorers.Team1 != null)
                        {
                            AwayScorers.ReplaceRange(matchLive.scorers.Team1);
                        }
                        if (matchLive.scorers.Team2 != null)
                        {
                            HomeScorers.ReplaceRange(matchLive.scorers.Team2);
                        }
                    }
					homeTeam = matchLive.tm.b2;
					awayTeam = matchLive.tm.a1;
                    AwayScoreString = matchLive.tm.a1.score.ToString();
                    HomeScoreString = matchLive.tm.b2.score.ToString();
				}
                if (matchLive.timeAdded != "")
                {
                    Clock = matchLive.timeAdded;
                } else {
                    Clock = matchLive.clock;
                }
                if (matchLive.inOT == 1){
					inOT = true;
                    Period = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_OVERTIME_ABBREV"); 
                } else {
                    if(matchLive.periodType == "PENALTYSHOOTOUT"){
                        Period = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_SHOOTOUT_ABBREV");                        
                    } else {
                        Period = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_PERIOD_ABBREV") + " " + matchLive.period;                        
                    }
				}
                if (GlobalVariables.Sport == "BASKETBALL")
                {
                    HomePeriodScores.Clear();
                    var homeP1 = new PeriodScore();
                    homeP1.sPoints = homeTeam.p1_score.ToString();
                    homeP1.periodType = "REGULAR";
                    homeP1.periodNumber = 1;
                    HomePeriodScores.Add(homeP1);

                    var homeP2 = new PeriodScore();
                    homeP2.sPoints = homeTeam.p2_score.ToString();
                    homeP2.periodType = "REGULAR";
                    homeP2.periodNumber = 2;
                    HomePeriodScores.Add(homeP2);

                    var homeP3 = new PeriodScore();
                    homeP3.sPoints = homeTeam.p3_score.ToString();
                    homeP3.periodType = "REGULAR";
                    homeP3.periodNumber = 3;
                    HomePeriodScores.Add(homeP3);

                    var homeP4 = new PeriodScore();
                    homeP4.sPoints = homeTeam.p4_score.ToString();
                    homeP4.periodType = "REGULAR";
                    homeP4.periodNumber = 4;
                    HomePeriodScores.Add(homeP4);
                    if (inOT)
                    {
                        var homeOT = new PeriodScore();
                        homeOT.sPoints = homeTeam.ot_score.ToString();
                        homeOT.periodType = "OVERTIME";
                        homeOT.periodNumber = 1;
                        HomePeriodScores.Add(homeOT);
                    }
                }
                if (GlobalVariables.Sport == "BASKETBALL")
                {
                    AwayPeriodScores.Clear();
                    var awayP1 = new PeriodScore();
                    awayP1.sPoints = awayTeam.p1_score.ToString();
                    awayP1.periodType = "REGULAR";
                    awayP1.periodNumber = 1;
                    AwayPeriodScores.Add(awayP1);

                    var awayP2 = new PeriodScore();
                    awayP2.sPoints = awayTeam.p2_score.ToString();
                    awayP2.periodType = "REGULAR";
                    awayP2.periodNumber = 2;
                    AwayPeriodScores.Add(awayP2);

                    var awayP3 = new PeriodScore();
                    awayP3.sPoints = awayTeam.p3_score.ToString();
                    awayP3.periodType = "REGULAR";
                    awayP3.periodNumber = 3;
                    AwayPeriodScores.Add(awayP3);

                    var awayP4 = new PeriodScore();
                    awayP4.sPoints = awayTeam.p4_score.ToString();
                    awayP4.periodType = "REGULAR";
                    awayP4.periodNumber = 4;
                    AwayPeriodScores.Add(awayP4);

                    if (inOT)
                    {
                        var awayOT = new PeriodScore();
                        awayOT.sPoints = awayTeam.ot_score.ToString();
                        awayOT.periodType = "OVERTIME";
                        awayOT.periodNumber = 1;
                        AwayPeriodScores.Add(awayOT);
                    }
                }

                LeadersData.Clear();
                var liveLeaderFields = GlobalVariables.leaderFields.ToList();
                for (var i = 0; i < liveLeaderFields.Count; i++)
                {
                    var block = new LiveLeadersBlockContent();
                    block.Title = AppResources.ResourceManager.GetString("STAT_PERSONCOMP_" + GlobalVariables.Sport + "_" + liveLeaderFields[i] + "_NAME");
                    LiveLeadersList leadersBlock = (LiveLeadersList)matchLive.totallds.GetType().GetRuntimeProperty(liveLeaderFields[i]).GetValue(matchLive.totallds, null);
                    var firstFive = new List<TotalLeader>();
                    if (leadersBlock != null)
                    {
                        if (leadersBlock.a1 != null)
                        {
                            firstFive.Add(leadersBlock.a1);
                        }
                        if (leadersBlock.b2 != null)
                        {
                            firstFive.Add(leadersBlock.b2);
                        }
                        if (leadersBlock.c3 != null)
                        {
                            firstFive.Add(leadersBlock.c3);
                        }
                        if (leadersBlock.d4 != null)
                        {
                            firstFive.Add(leadersBlock.d4);
                        }
                        if (leadersBlock.e5 != null)
                        {
                            firstFive.Add(leadersBlock.e5);
                        }
                        foreach (var currentItem in firstFive)
                        {
                            if (currentItem != null)
                            {
                                if (currentItem.tno == 1)
                                {
                                    currentItem.teamName = matchLive.tm.a1.name;
                                }
                                else
                                {
                                    currentItem.teamName = matchLive.tm.b2.name;
                                }
                            }
                        }
                        block.Leaders = firstFive;
                        if (block.Leaders.Count() > 0)
                        {
                            if (firstFive[0].tot > 0)
                            {
                                LeadersData.Add(block);
                            }
                        }
                    }
				}
				LeadersDataList.Clear();
				LeadersDataList.AddRange(LeadersData.ToList());

				if (liveLeaderFields.Count > 1)
				{
					Position = 1;
					await DelayCarousel(500);
				}

                ComparisonStats.Clear();
                var liveCompareFields = GlobalVariables.compareFields.ToList();

                foreach (var currentItem in liveCompareFields)
                {
                    var newLine = new StatBar();
                    newLine.title = AppResources.ResourceManager.GetString("STAT_TEAMMATCH_" + GlobalVariables.Sport + "_" + currentItem.ToString() + "_NAME");
                    newLine.IsLiveMatch = true;
                    newLine.IsPercent = false;
                    if (currentItem.Contains("Percent"))
                    {
                        newLine.IsPercent = true;
                    }
                    newLine.team1 = Convert.ToDouble(homeTeam.GetType().GetRuntimeProperty("tot_" + currentItem).GetValue(homeTeam, null));
                    newLine.team2 = Convert.ToDouble(awayTeam.GetType().GetRuntimeProperty("tot_" + currentItem).GetValue(awayTeam, null));
                    ComparisonStats.Add(newLine);
                }

			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);
				MessagingCenter.Send(new MessagingCenterAlert
				{
					Title = "Error",
					Message = "Unable to load items.",
					Cancel = "OK"
				}, "message");
			}
			finally
			{
				IsBusy = false;
				IsNotBusy = true;
			}

		}
	}
}