﻿using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Linq;
using System.Reactive.Linq;

using Xamarin.Forms;

namespace HostedMobile
{
    public class BaseViewModel : ObservableObject
    {
        public BaseViewModel()
        {
			PageButtons = new ObservableRangeCollection<PageButton>();
            pageCommand = new Command(PageTapped);
            UpdatePagePosition = new Command(ExecuteUpdatePagePosition);
			PageButtons.Clear();
		}

        bool isBusy = false;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

		bool isNotBusy = true;
		public bool IsNotBusy
        {
			get { return isNotBusy; }
			set { SetProperty(ref isNotBusy, value); }
		}

        string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

		bool hasRounds = false;
		public bool HasRounds
		{
			get { return hasRounds; }
			set { SetProperty(ref hasRounds, value); }
		}

        bool hasDates = false;
        public bool HasDates
        {
            get { return hasDates; }
            set { SetProperty(ref hasDates, value); }
        }

		bool hasPhases = false;
		public bool HasPhases
		{
			get { return hasPhases; }
			set { SetProperty(ref hasPhases, value); }
		}

        bool hasPools = false;
		public bool HasPools
		{
			get { return hasPools; }
			set { SetProperty(ref hasPools, value); }
		}

        bool hasMatchTypes = false;
		public bool HasMatchTypes
		{
			get { return hasMatchTypes; }
			set { SetProperty(ref hasMatchTypes, value); }
		}

        bool hasHeadToHead = false;
        public bool HasHeadToHead
        {
            get { return hasHeadToHead; }
            set { SetProperty(ref hasHeadToHead, value); }
        }

        bool showNextRound = false;
		public bool ShowNextRound
		{
			get { return showNextRound; }
			set { SetProperty(ref showNextRound, value); }
		}

		bool showPreviousRound = false;
		public bool ShowPreviousRound
		{
			get { return showPreviousRound; }
			set { SetProperty(ref showPreviousRound, value); }
		}

		bool sameComp = true;
		public bool SameComp
		{
			get { return sameComp; }
			set { SetProperty(ref sameComp, value); }
		}

		string period = "";
		public string Period
		{
			get { return period; }
			set { SetProperty(ref period, value); }
		}
		string clock = "";
		public string Clock
		{
			get { return clock; }
			set { SetProperty(ref clock, value); }
		}

		string homeScoreString = "";
		public string HomeScoreString
		{
			get { return homeScoreString; }
			set { SetProperty(ref homeScoreString, value); }
		}
		string awayScoreString = "";
		public string AwayScoreString
		{
			get { return awayScoreString; }
			set { SetProperty(ref awayScoreString, value); }
		}

        string homeScoreSecondaryString = "";
        public string HomeScoreSecondaryString
        {
            get { return homeScoreSecondaryString; }
            set { SetProperty(ref homeScoreSecondaryString, value); }
        }
        string awayScoreSecondaryString = "";
        public string AwayScoreSecondaryString
        {
            get { return awayScoreSecondaryString; }
            set { SetProperty(ref awayScoreSecondaryString, value); }
        }

		bool inOt = true;
		public bool inOT
		{
			get { return inOt; }
			set { SetProperty(ref inOt, value); }
		}

        bool isFootball = false;
        public bool isFOOTBALL
        {
            get { return isFootball; }
            set { SetProperty(ref isFootball, value); }
        }

        bool isBasketball = false;
        public bool isBASKETBALL
        {
            get { return isBasketball; }
            set { SetProperty(ref isBasketball, value); }
        }

        private int _position;
		public int Position { 
            get { return _position; } 
            set { _position = value; OnPropertyChanged(); } }

		private int _pageposition;
		public int PagePosition
		{
			get { return _pageposition; }
			set { _pageposition = value; OnPropertyChanged(); }
		}

		public async Task DelayCarousel(int delay)
		{
			await Task.Delay(delay);

			Position = 0;
		}

		public ObservableRangeCollection<PageButton> PageButtons { get; set; }

        public Command UpdatePagePosition { get; set; }
		public ICommand pageCommand;

		public void ExecuteUpdatePagePosition()
		{
			if (PageButtons.Count > 1)
			{
                for (int i = 0; i < PageButtons.Count; i++)
				{
					PageButtons[i].Current = false;
				}
				PageButtons[PagePosition].Current = true;
			}
		}

		public ICommand PageCommand
		{
			get { return pageCommand; }
		}

		public void PageTapped(object s)
		{
			var tappedButton = (PageButton)s;
            PagePosition = PageButtons.IndexOf(PageButtons.Where(p => p.ButtonText == tappedButton.ButtonText).FirstOrDefault());
			ExecuteUpdatePagePosition();

		}

	}
}
