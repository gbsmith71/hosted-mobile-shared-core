﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HostedMobile.Core.ResX;
using System.Windows.Input;
using Xamarin.Forms;

namespace HostedMobile
{
	public class StandingsViewModel : BaseViewModel
	{
		public ObservableRangeCollection<Standing> StandingsList { get; set; }
		public ObservableRangeCollection<Standing> StandingsList2 { get; set; }
		public ObservableRangeCollection<String> Comps { get; set; }
		public Command LoadStandingsCommand { get; set; }
		public int CurrentComp { get; set; }
		public string CurrentCompName { get; set; }
		public List<Competition> CompList;
		public List<String> compList;
		public String[] FieldList;
		public int roundIndex;
		public ObservableRangeCollection<PoolDetail> PoolList { get; set; }
		public ObservableRangeCollection<PhaseDetail> PhaseList { get; set; }
		public ObservableRangeCollection<MatchType> MatchTypeList { get; set; }
		public ObservableRangeCollection<RoundDetail> RoundList { get; set; }
		public DateTime StandingsUpdated;
		ICommand poolCommand;
		ICommand phaseCommand;
		ICommand matchTypeCommand;

		public StandingsViewModel()
		{
			Title = AppResources.ResourceManager.GetString("Standings");
			StandingsList = new ObservableRangeCollection<Standing>();
			StandingsList2 = new ObservableRangeCollection<Standing>();
			PoolList = new ObservableRangeCollection<PoolDetail>();
			PhaseList = new ObservableRangeCollection<PhaseDetail>();
			RoundList = new ObservableRangeCollection<RoundDetail>();
			MatchTypeList = new ObservableRangeCollection<MatchType>();
			Comps = new ObservableRangeCollection<String>();
			LoadStandingsCommand = new Command (async () => await ExecuteLoadStandingsCommand());
			poolCommand = new Command(PoolTapped);
			phaseCommand = new Command(PhaseTapped);
			matchTypeCommand = new Command(MatchTypeTapped);
			roundIndex = new int();

		}

		async Task ExecuteLoadStandingsCommand()
		{
			if (IsBusy)
				return;

			IsBusy = true;
			IsNotBusy = false;

			try
			{
                SameComp = CurrentCompName == Settings.CurrentComp ? true : false;

                var hs = new HostedDataService();
                var standingsList = await hs.GetStandings(true);

                if (Settings.CurrentComp != string.Empty)
                {
                    Settings.CurrentComp = standingsList.competitionId;
                    Settings.CurrentPhase = standingsList.current_phaseName;
                    Settings.CurrentMatchType = standingsList.current_matchType;
                    Settings.CurrentPool = standingsList.current_poolNumber.ToString();

                    for (var i = 0; i < standingsList.standings.Count; i++)
                    {
                        standingsList.standings[i].index = i + 1;
                    }
                    //var orderedList = standingsList.standings.OrderBy(listteam => listteam.index);
                    //var orderedList = standingsList.standings;

                    var dataMatch = Compare.JsonCompare(StandingsList, standingsList.standings);
                    if (!dataMatch)
                    {
                        PoolList.Clear();
                        PoolList.ReplaceRange(standingsList.poolDetails);
                        PhaseList.Clear();
                        PhaseList.ReplaceRange(standingsList.phases);
                        MatchTypeList.Clear();
                        MatchTypeList.ReplaceRange(standingsList.matchTypes);
                        StandingsList.Clear();
                        StandingsList.ReplaceRange(standingsList.standings);
                        StandingsList2.Clear();
                        StandingsList2.ReplaceRange(standingsList.standings);
                    }
				}

				CompList = standingsList.FilterLists.competitions;
                FieldList = standingsList.fieldOrder.ToArray();

                //Load Comp Picker
                if (Comps.Count == 0)
                {
                    Comps.Clear();
                    compList = hs.GetFilterComps(standingsList.FilterLists);
                    Comps.ReplaceRange(compList);
                }
                CompSelectedIndex = hs.GetFilterCompsSelected(standingsList.FilterLists, standingsList.competitionId);

                HasPools = standingsList.poolDetails.Count > 1 ? true : false;
                HasPhases = standingsList.phases.Count > 1 ? true : false;
                HasMatchTypes = standingsList.matchTypes.Count > 1 ? true : false;
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);
				MessagingCenter.Send(new MessagingCenterAlert
				{
					Title = "Error",
					Message = "Unable to load items.",
					Cancel = "OK"
				}, "message");
			}
			finally
			{
				IsBusy = false;
				IsNotBusy = true;

			}

		}


		int compSelectedIndex;
		public int CompSelectedIndex
		{
			get
			{
				return compSelectedIndex;
			}
			set
			{
				if (compSelectedIndex != value)
				{
					compSelectedIndex = value;
					OnPropertyChanged(nameof(CompSelectedIndex));
					if (Settings.CurrentComp != CompList[compSelectedIndex].competitionId.ToString())
					{
						Settings.CurrentComp = CompList[compSelectedIndex].competitionId.ToString();
						CurrentCompName = CompList[compSelectedIndex].competitionId.ToString();
						ClearCurrents();
						HasPools = false;
						HasPhases = false;
						HasRounds = false;
						HasMatchTypes = false;
						StandingsList.Clear();
						SameComp = false;
						LoadStandingsCommand.Execute(null);
					}
                } else {
					compSelectedIndex = value;
					OnPropertyChanged(nameof(CompSelectedIndex));
				}
			}
		}

		public ICommand PoolCommand
		{
			get { return poolCommand; }
		}

		void PoolTapped(object s)
		{
			PoolDetail pool = (PoolDetail)s;
			ClearCurrents();
			Settings.CurrentPool = pool.poolId;
			LoadStandingsCommand.Execute(null);
		}

		public ICommand PhaseCommand
		{
			get { return phaseCommand; }
		}

		void PhaseTapped(object s)
		{
			PhaseDetail phase = (PhaseDetail)s;
			ClearCurrents();
			Settings.CurrentPhase = phase.name;
			LoadStandingsCommand.Execute(null);
		}
		public ICommand MatchTypeCommand
		{
			get { return matchTypeCommand; }
		}

		void MatchTypeTapped(object s)
		{
			MatchType matchType = (MatchType)s;
            ClearCurrents();
            Settings.CurrentMatchType = matchType.matchType;
			LoadStandingsCommand.Execute(null);
		}

        void ClearCurrents() {
            Settings.CurrentPool = String.Empty;
			Settings.CurrentPhase = String.Empty;
			Settings.CurrentMatchType = String.Empty;
            Settings.CurrentDate = String.Empty;
			Settings.CurrentRound = String.Empty;
			Settings.CurrentRoundName = String.Empty;
        }
	}
}
