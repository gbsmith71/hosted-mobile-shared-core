﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using HostedMobile.Core.ResX;
using System.Windows.Input;

using Xamarin.Forms;

namespace HostedMobile
{
	public class TeamsViewModel : BaseViewModel
	{
		public ObservableRangeCollection<Team> Teams { get; set; }
		public ObservableRangeCollection<String> Comps { get; set; }
		public Command LoadTeamsCommand { get; set; }
		public int CurrentComp { get; set; }
		public List<Competition> CompList;
		public List<String> compList;
        public DateTime TeamsUpdated;
        public INavigation _navigation;

        ICommand loadTeam;

		public TeamsViewModel(INavigation navigation)
		{
			Title = AppResources.ResourceManager.GetString("Teams");
			Teams = new ObservableRangeCollection<Team>();
			Comps = new ObservableRangeCollection<String>();
			LoadTeamsCommand = new Command(async () => await ExecuteLoadTeamsCommand());
            loadTeam = new Command(LoadTeamTapped);
            _navigation = navigation;
		}

		async Task ExecuteLoadTeamsCommand()
		{
			if (IsBusy)
				return;

			IsBusy = true;
            IsNotBusy = false;

			try
			{
				var hs = new HostedDataService();
				var teamList = await hs.GetTeams(true);
                for (var i = 0; i < teamList.teams.Count; i++)
				{
					teamList.teams[i].index = i + 1;
				}

				var dataMatch = Compare.JsonCompare(Teams, teamList.teams);
				if (!dataMatch)
				{
					Teams.Clear();
					Teams.ReplaceRange(teamList.teams);
				}
				CompList = teamList.FilterLists.competitions;

				//Load Comp Picker
				if (Comps.Count == 0)
				{
					Comps.Clear();
					compList = hs.GetFilterComps(teamList.FilterLists);
					Comps.ReplaceRange(compList);
				}
				CompSelectedIndex = hs.GetFilterCompsSelected(teamList.FilterLists, teamList.competitionId);
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);
				MessagingCenter.Send(new MessagingCenterAlert
				{
					Title = "Error",
					Message = "Unable to load items.",
					Cancel = "OK"
				}, "message");
			}
			finally
			{
				IsBusy = false;
				IsNotBusy = true;

			}

		}

		int compSelectedIndex;
		public int CompSelectedIndex
		{
			get
			{
				return compSelectedIndex;
			}
			set
			{
				if (compSelectedIndex != value)
				{
					compSelectedIndex = value;
					OnPropertyChanged(nameof(CompSelectedIndex));
					if (Settings.CurrentComp != CompList[compSelectedIndex].competitionId.ToString())
					{
						Settings.CurrentComp = CompList[compSelectedIndex].competitionId.ToString();
						Settings.CurrentRound = String.Empty;
                        Settings.CurrentDate = String.Empty;
						Teams.Clear();
						LoadTeamsCommand.Execute(null);
                    }
				} else {
					compSelectedIndex = value;
					OnPropertyChanged(nameof(CompSelectedIndex));
				}

			}
		}
		public ICommand LoadTeam
		{
			get { return loadTeam; }
		}

		void LoadTeamTapped(object s)
		{
            Team team = (Team)s;
            _navigation.PushAsync(new TeamRosterPage(team));
		}

	}
}
