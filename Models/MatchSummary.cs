﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
namespace HostedMobile
{

	public class SummaryBoxscore : ObservableObject
	{
		public int periodNumber { get; set; }
		public string periodType { get; set; }
		public int matchId { get; set; }
		public int personId { get; set; }
		public int teamId { get; set; }
		public int competitionId { get; set; }
		public int leagueId { get; set; }
		public string externalId { get; set; }
		public string shirtNumber { get; set; }
		public string playingPosition { get; set; }
		public int participated { get; set; }
		public string DNPReason { get; set; }
		public int isPlayer { get; set; }
		public int isTeamOfficial { get; set; }
		public int isStarter { get; set; }
		public string updated { get; set; }
		public int sPointsInThePaint { get; set; }
		public int sPointsInThePaintAttempted { get; set; }
		public int sPointsInThePaintMade { get; set; }
		public int sPointsSecondChance { get; set; }
		public int sPointsFastBreak { get; set; }
		public int sPoints { get; set; }
		public int sMVPVotes { get; set; }
		public int sPER { get; set; }
		public int sPlus { get; set; }
		public int sReboundsDefensive { get; set; }
		public int sReboundsOffensive { get; set; }
		public int sTurnovers { get; set; }
		public int sTwoPointersMade { get; set; }
		public int sTwoPointersAttempted { get; set; }
		public int sThreePointersMade { get; set; }
		public int sThreePointersAttempted { get; set; }
		public int sSecondChancePointsAttempted { get; set; }
		public int sSecondChancePointsMade { get; set; }
		public int sSteals { get; set; }
		public double sMinutes { get; set; }
		public int sMinus { get; set; }
		public int sFastBreakPointsMade { get; set; }
		public int sFieldGoalsAttempted { get; set; }
		public int sFieldGoalsMade { get; set; }
		public int sFastBreakPointsAttempted { get; set; }
		public int sEfficiencyCustom { get; set; }
		public int sAssistsDefensive { get; set; }
		public int sBlocks { get; set; }
		public int sBlocksReceived { get; set; }
		public int sFoulsCoachDisqualifying { get; set; }
		public int sFoulsCoachTechnical { get; set; }
		public int sFoulsUnsportsmanlike { get; set; }
		public int sFreeThrowsAttempted { get; set; }
		public int sFreeThrowsMade { get; set; }
		public int sFoulsTechnical { get; set; }
		public int sFoulsPersonal { get; set; }
		public int sFoulsDisqualifying { get; set; }
		public int sFoulsOffensive { get; set; }
		public int sFoulsOn { get; set; }
		public int sAssists { get; set; }
		public double sThreePointersPercentage { get; set; }
		public double sStealsPercentage { get; set; }
		public double sSecondChancePointsPercentage { get; set; }
		public double sTrueShootingAttempts { get; set; }
		public double sTurnoversPercentage { get; set; }
		public int sDefensiveRating { get; set; }
		public int sOffensiveRating { get; set; }
		public double sTwoPointersPercentage { get; set; }
		public int sReboundsTotal { get; set; }
		public double sReboundsOffensivePercentage { get; set; }
		public double sFieldGoalsEffectivePercentage { get; set; }
		public double sFastBreakPointsPercentage { get; set; }
		public double sEfficiencyGameScore { get; set; }
		public double sFieldGoalsPercentage { get; set; }
		public double sFreeThrowsPercentage { get; set; }
		public double sReboundsDefensivePercentage { get; set; }
		public double sPointsInThePaintPercentage { get; set; }
		public int sPlusMinusPoints { get; set; }
		public double sAssistsTurnoverRatio { get; set; }
		public double sTrueShootingPercentage { get; set; }
		public double sReboundsPercentage { get; set; }
		public int sEfficiency { get; set; }
		public string linkDetail { get; set; }
		public string linkDetailMatch { get; set; }
		public string linkDetailTeam { get; set; }
		public string linkDetailPerson { get; set; }
		public string linkDetailLeague { get; set; }
		public string leagueName { get; set; }
		public string teamName { get; set; }
		public string teamNameInternational { get; set; }
		public string personName { get; set; }
		public string firstName { get; set; }
		public string familyName { get; set; }
		public string TVName { get; set; }
		public string scoreboardName { get; set; }
		public string nickName { get; set; }
		public string internationalFirstName { get; set; }
		public string internationalFamilyName { get; set; }
		public string website { get; set; }
		public string internationalReference { get; set; }
		public string nationalityCodeIOC { get; set; }
		public string nationalityCode { get; set; }
		public string nationality { get; set; }
		public Images images { get; set; }
		public string linkDetailCompetitionPlayer { get; set; }
	}

	public class PeriodScore : ObservableObject
	{
		public int periodNumber { get; set; }
		public string periodType { get; set; }
		public string teamId { get; set; }
		public string sPoints { get; set; }
		public string sGoals { get; set; }
		public string score { 
            get {
                if (sPoints == null){
                    return sGoals;
                } else {
					return sPoints;
				}
            }
        }
	}
	public class Periodbyperiod
	{
        public List<PeriodScore> homeperiods { get; set; }
		public List<PeriodScore> awayperiods { get; set; }
	}


	public class MatchSummary
    {
		public List<string> leadersFieldOrder { get; set; }
		public List<string> compareFieldOrder { get; set; }
		public Match matchDetail { get; set; }
		public string competitionId { get; set; }
		public Periodbyperiod periodbyperiod { get; set; }
        public List<string> boxScoreFieldOrder { get; set; }
		public List<SummaryBoxscore> boxscore_hometeam { get; set; }
		public List<SummaryBoxscore> boxscore_awayteam { get; set; }
		public FilterLists FilterLists { get; set; }
		public string matchpage { get; set; }
		public List<ComparisonStat> comparisonStats { get; set; }
        public List<List<LeaderDetails>> leaderData { get; set; }
		public Common common { get; set; }
		public Config config { get; set; }
    }
}
