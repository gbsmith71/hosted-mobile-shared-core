﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace HostedMobile
{
    public class Head2head : ObservableObject
    {
        public int leagueId { get; set; }
        public int competitionId { get; set; }
        public string competitionName { get; set; }
        public int matchId { get; set; }
        public string externalId { get; set; }
        public string matchType { get; set; }
        public string matchStatus { get; set; }
        public string matchTime { get; set; }
        public string matchTimeUTC { get; set; }
        public DateTime shortDate {
            get {
                DateTime returnDate = Convert.ToDateTime(matchTime);
                return returnDate;
            }
        }

        public int extraPeriodsUsed { get; set; }
        public string linkDetail { get; set; }
        public List<Competitor> competitors { get; set; }
        public Competitor homeTeam { 
            get{
                if(competitors[0].isHomeCompetitor == 1){
                    return competitors[0];
                } else {
                    return competitors[1]; 
                }
            } 
        }
		public Competitor awayTeam {
			get
			{
				if (competitors[0].isHomeCompetitor == 1)
				{
					return competitors[1];
				}
				else
				{
					return competitors[0];
				}
			}
		}
		public int index { get; set; }
		public Color RowColor
		{
			get
			{
				if (index % 2 == 0)
					return (Color)App.Current.Resources["TableStripe"];
				else
					return Color.White;
			}
		}
	}

    public class MatchHistory : ObservableObject
    {
        public int matchId { get; set; }
        public string matchTime { get; set; }
        public string score1 { get; set; }
        public string score2 { get; set; }
        public string team2 { get; set; }
        public string team2logo { get; set; }
        public string resultString { get; set; }
        public int isHomeCompetitor { get; set; }
    }
            
    public class LeaderDetails : ObservableObject
    {
        public string personId { get; set; }
        public string competitionId { get; set; }
        public string leagueId { get; set; }
        public string teamId { get; set; }
        public string value { get; set; }
        public string sBlocks { get; set; }
        public string linkDetailCompetition { get; set; }
        public string leagueName { get; set; }
        public string linkDetailLeague { get; set; }
        public string personName { get; set; }
        public string firstName { get; set; }
        public string familyName { get; set; }
        public string TVName { get; set; }
        public string scoreboardName { get; set; }
        public string website { get; set; }
        public string nickName { get; set; }
        public string externalId { get; set; }
        public string internationalReference { get; set; }
        public string internationalFirstName { get; set; }
        public string internationalFamilyName { get; set; }
        public string defaultShirtNumber { get; set; }
        public string defaultPlayingPosition { get; set; }
        public string linkDetailPerson { get; set; }
        public string primaryClubName { get; set; }
        public string linkDetailPrimaryClub { get; set; }
        public Images images { get; set; }
        public string teamName { get; set; }
        public string linkDetailTeam { get; set; }
        public string linkDetailCompetitionPlayer { get; set; }
        public int teamNumber { get; set; }
    }

    public class StatBar : ObservableObject
    {
        public string title { get; set; }
        public double team1 { get; set; }
        public double team2 { get; set; }
        public bool IsLiveMatch { get; set; }
		public bool IsPercent { get; set; }
		public double team1output {
            get {
                if (IsPercent && !IsLiveMatch)
				{
                    return team1 * 100;
				
                } else {
                    return team1;
                }
            }
        }
		public double team2output
		{
			get
			{
				if (IsPercent && !IsLiveMatch)
				{
					return team2 * 100;

				}
				else
				{
					return team2;
				}
			}
		}
		public double total
        {
            get
            {
				var val = team1 + team2;
                if (val <= 0)
                {
                    val = 1;
                }
				return val;
            }
        }
        public GridLength team1percentage
        {
            get
            {
				var val = (int)((team1 / total) * 100);
				if (val <= 0)
				{
					val = 1;
				}
				return new GridLength(val, GridUnitType.Star);
            }
        }
        public GridLength team2percentage
        {
            get
            {
                var val = (int)((team2 / total) * 100);
				if (val <= 0)
				{
					val = 1;
				}
				return new GridLength((val), GridUnitType.Star);
            }
        }
    }

	public class ComparisonStat : ObservableObject
    {
        public int periodNumber { get; set; }
        public string periodType { get; set; }
        public int competitionId { get; set; }
        public int teamId { get; set; }
        public int leagueId { get; set; }
        public string homeAway { get; set; }
        public string winLoss { get; set; }
        public string inConference { get; set; }
        public string matchType { get; set; }
        public int sReboundsDefensive { get; set; }
        public double sReboundsDefensiveAverage { get; set; }
        public double sPossessionsOpponent { get; set; }
        public double sPossessionsOpponentAverage { get; set; }
        public int sReboundsOffensive { get; set; }
        public double sReboundsOffensiveAverage { get; set; }
        public int sReboundsTeamDefensive { get; set; }
        public int sReboundsTeamOffensive { get; set; }
        public double sPossessions { get; set; }
        public double sPossessionsAverage { get; set; }
        public int sPointsSecondChance { get; set; }
        public double sPointsSecondChanceAverage { get; set; }
        public int sPointsFromTurnovers { get; set; }
        public double sPointsFromTurnoversAverage { get; set; }
        public int sPointsFastBreak { get; set; }
        public double sPointsFastBreakAverage { get; set; }
        public int sPointsInThePaint { get; set; }
        public double sPointsInThePaintAverage { get; set; }
        public int sPointsInThePaintAttempted { get; set; }
        public double sPointsInThePaintAttemptedAverage { get; set; }
        public int sPointsInThePaintMade { get; set; }
        public double sPointsInThePaintMadeAverage { get; set; }
        public string sResults { get; set; }
        public int sSecondChancePointsAttempted { get; set; }
        public double sSecondChancePointsAttemptedAverage { get; set; }
        public int sTurnoversTeam { get; set; }
        public double sTurnoversTeamAverage { get; set; }
        public int sTurnovers { get; set; }
        public double sTurnoversAverage { get; set; }
        public int sTwoPointersAttempted { get; set; }
        public double sTwoPointersAttemptedAverage { get; set; }
        public int sTwoPointersMade { get; set; }
        public double sTwoPointersMadeAverage { get; set; }
        public int sWins { get; set; }
        public double sWinsAverage { get; set; }
        public double sTransitionOffence { get; set; }
        public double sTransitionOffenceAverage { get; set; }
        public double sTransitionDefence { get; set; }
        public double sTransitionDefenceAverage { get; set; }
        public int sSteals { get; set; }
        public double sStealsAverage { get; set; }
        public int sSecondChancePointsMade { get; set; }
        public double sSecondChancePointsMadeAverage { get; set; }
        public int sThreePointersAttempted { get; set; }
        public double sThreePointersAttemptedAverage { get; set; }
        public int sThreePointersMade { get; set; }
        public double sThreePointersMadeAverage { get; set; }
        public double sTimeLeading { get; set; }
        public int sPointsAgainst { get; set; }
        public double sPointsAgainstAverage { get; set; }
        public double sMinutes { get; set; }
        public double sMinutesAverage { get; set; }
        public int sFieldGoalsAttempted { get; set; }
        public double sFieldGoalsAttemptedAverage { get; set; }
        public int sFastBreakPointsMade { get; set; }
        public double sFastBreakPointsMadeAverage { get; set; }
        public int sFieldGoalsMade { get; set; }
        public double sFieldGoalsMadeAverage { get; set; }
        public int sForfeitsAgainst { get; set; }
        public double sForfeitsAgainstAverage { get; set; }
        public int sForfeitsFor { get; set; }
        public double sForfeitsForAverage { get; set; }
        public int sFastBreakPointsAttempted { get; set; }
        public double sFastBreakPointsAttemptedAverage { get; set; }
        public double sEfficiencyCustom { get; set; }
        public int sBenchPoints { get; set; }
        public double sBenchPointsAverage { get; set; }
        public int sAssistsDefensive { get; set; }
        public double sAssistsDefensiveAverage { get; set; }
        public int sBlocks { get; set; }
        public double sBlocksAverage { get; set; }
        public int sBlocksReceived { get; set; }
        public double sBlocksReceivedAverage { get; set; }
        public int sDraws { get; set; }
        public double sDrawsAverage { get; set; }
        public int sFoulsBenchTechnical { get; set; }
        public double sFoulsBenchTechnicalAverage { get; set; }
        public int sFoulsCoachDisqualifying { get; set; }
        public double sFoulsCoachDisqualifyingAverage { get; set; }
        public int sFreeThrowsAttempted { get; set; }
        public double sFreeThrowsAttemptedAverage { get; set; }
        public int sFoulsUnsportsmanlike { get; set; }
        public double sFoulsUnsportsmanlikeAverage { get; set; }
        public int sFreeThrowsMade { get; set; }
        public double sFreeThrowsMadeAverage { get; set; }
        public int sGames { get; set; }
        public int sLosses { get; set; }
        public double sLossesAverage { get; set; }
        public int sFoulsTechnicalOther { get; set; }
        public double sFoulsTechnicalOtherAverage { get; set; }
        public int sFoulsTechnical { get; set; }
        public double sFoulsTechnicalAverage { get; set; }
        public int sFoulsDisqualifying { get; set; }
        public double sFoulsDisqualifyingAverage { get; set; }
        public int sFoulsCoachTechnical { get; set; }
        public double sFoulsCoachTechnicalAverage { get; set; }
        public int sFoulsOn { get; set; }
        public double sFoulsOnAverage { get; set; }
        public int sFoulsOffensive { get; set; }
        public double sFoulsOffensiveAverage { get; set; }
        public int sFoulsPersonal { get; set; }
        public double sFoulsPersonalAverage { get; set; }
        public int sAssists { get; set; }
        public double sAssistsAverage { get; set; }
        public int sReboundsPersonal { get; set; }
        public double sReboundsPersonalAverage { get; set; }
        public double sPointsInThePaintPercentage { get; set; }
        public int sPoints { get; set; }
        public double sPointsAverage { get; set; }
        public int sReboundsTeam { get; set; }
        public double sReboundsTeamAverage { get; set; }
        public double sSecondChancePointsPercentage { get; set; }
        public double sTwoPointersPercentage { get; set; }
        public double sThreePointersPercentage { get; set; }
        public double sPace { get; set; }
        public double sPaceAverage { get; set; }
        public double sFreeThrowsPercentage { get; set; }
        public double sFastBreakPointsPercentage { get; set; }
        public double sDefensiveRating { get; set; }
        public double sDefensiveRatingAverage { get; set; }
        public double sDefensivePointsPerPossession { get; set; }
        public double sDefensivePointsPerPossessionAverage { get; set; }
        public double sFieldGoalsEffectiveAdjusted { get; set; }
        public double sFieldGoalsEffectiveAdjustedAverage { get; set; }
        public double sFieldGoalsPercentage { get; set; }
        public double sFieldGoalsPercentageAverage { get; set; }
        public int sFoulsTotal { get; set; }
        public double sFoulsTotalAverage { get; set; }
        public int sFoulsTeam { get; set; }
        public double sFoulsTeamAverage { get; set; }
        public double sAssistsTurnoverRatio { get; set; }
        public int sReboundsTotal { get; set; }
        public double sReboundsTotalAverage { get; set; }
        public int sPlusMinus { get; set; }
        public double sPlusMinusAverage { get; set; }
        public double sOffensiveRating { get; set; }
        public double sOffensiveRatingAverage { get; set; }
        public double sOffensivePointsPerPossession { get; set; }
        public double sOffensivePointsPerPossessionAverage { get; set; }
        public int sEfficiency { get; set; }
        public int sPenaltyShootOutAttempted { get; set; }
		public double sPenaltyShootOutAttemptedAverage { get; set; }
		public int sPassingDistanceCovered { get; set; }
		public double sPassingDistanceCoveredAverage { get; set; }
		public int sPenaltyShootOutMade { get; set; }
		public double sPenaltyShootOutMadeAverage { get; set; }
		public int sPenaltyShootOutSave { get; set; }
		public double sPenaltyShootOutSaveAverage { get; set; }
		public int sPenaltyShotsMade { get; set; }
		public double sPenaltyShotsMadeAverage { get; set; }
		public int sPenaltyShotsAttempted { get; set; }
		public double sPenaltyShotsAttemptedAverage { get; set; }
		public int sPassesKey { get; set; }
		public double sPassesKeyAverage { get; set; }
		public int sPassesCompleted { get; set; }
		public double sPassesCompletedAverage { get; set; }
		public int sOffsides { get; set; }
		public double sOffsidesAverage { get; set; }
		public int sOwnGoalsAgainst { get; set; }
		public double sOwnGoalsAgainstAverage { get; set; }
		public int sPasses { get; set; }
		public double sPassesAverage { get; set; }
		public int sOwnGoalsFor { get; set; }
		public double sOwnGoalsForAverage { get; set; }
		public int sRedCards { get; set; }
		public double sRedCardsAverage { get; set; }
		public int sTacklesLost { get; set; }
		public double sTacklesLostAverage { get; set; }
		public int sTackles { get; set; }
		public double sTacklesAverage { get; set; }
		public int sTacklesWon { get; set; }
		public double sTacklesWonAverage { get; set; }
		public int sThrowIns { get; set; }
		public double sThrowInsAverage { get; set; }
		public int sYellowCards { get; set; }
		public double sYellowCardsAverage { get; set; }
		public int sShotsSaved { get; set; }
		public double sShotsSavedAverage { get; set; }
		public int sShotsOutsideBox { get; set; }
		public double sShotsOutsideBoxAverage { get; set; }
		public int sShotsBlocked { get; set; }
		public double sShotsBlockedAverage { get; set; }
		public int sShots { get; set; }
		public double sShotsAverage { get; set; }
		public int sShotsInsideBox { get; set; }
		public double sShotsInsideBoxAverage { get; set; }
		public int sShotsOffTarget { get; set; }
		public double sShotsOffTargetAverage { get; set; }
		public int sShotsOnWoodwork { get; set; }
		public double sShotsOnWoodworkAverage { get; set; }
		public int sShotsOnTarget { get; set; }
		public double sShotsOnTargetAverage { get; set; }
		public int sInterceptions { get; set; }
		public double sInterceptionsAverage { get; set; }
		public int sGoalsRightFoot { get; set; }
		public double sGoalsRightFootAverage { get; set; }
		public int sClearances { get; set; }
		public double sClearancesAverage { get; set; }
		public int sCleanSheets { get; set; }
		public double sCleanSheetsAverage { get; set; }
		public int sClearancesHeader { get; set; }
		public double sClearancesHeaderAverage { get; set; }
		public int sCorners { get; set; }
		public double sCornersAverage { get; set; }
		public int sCrossesCompleted { get; set; }
		public double sCrossesCompletedAverage { get; set; }
		public int sCrosses { get; set; }
		public int sCrossesAverage { get; set; }
		public int sClaimedSmother { get; set; }
		public double sClaimedSmotherAverage { get; set; }
		public int sClaimedCrosses { get; set; }
		public double sClaimedCrossesAverage { get; set; }
		public int sBallsRecovered { get; set; }
		public double sBallsRecoveredAverage { get; set; }
		public int sChancesCreated { get; set; }
		public double sChancesCreatedAverage { get; set; }
		public int sDuels { get; set; }
		public int sDuelsAverage { get; set; }
		public int sGoalsConceded { get; set; }
		public double sGoalsConcededAverage { get; set; }
		public int sGoals { get; set; }
		public double sGoalsAverage { get; set; }
		public int sGoalsFreeKick { get; set; }
		public double sGoalsFreeKickAverage { get; set; }
		public int sGoalsHeader { get; set; }
		public double sGoalsHeaderAverage { get; set; }
		public int sGoalsPenaltyKick { get; set; }
		public double sGoalsPenaltyKickAverage { get; set; }
		public int sGoalsLeftFoot { get; set; }
		public int sGoalsLeftFootAverage { get; set; }
		public int sFreeKicks { get; set; }
		public double sFreeKicksAverage { get; set; }
		public int sFoulsWon { get; set; }
		public double sFoulsWonAverage { get; set; }
		public int sDuelsWon { get; set; }
		public double sDuelsWonAverage { get; set; }
		public int sDuelsLost { get; set; }
		public double sDuelsLostAverage { get; set; }
		public int sFoulsCommitted { get; set; }
		public double sFoulsCommittedAverage { get; set; }
        public double sPenaltyShotsPercentage { get; set; }
		public double sPenaltyShotsPercentageAverage { get; set; }
        public double sShotsOnTargetPercentage { get; set; }
		public double sShotsOnTargetPercentageAverage { get; set; }
        public double sTacklesPercentage { get; set; }
		public double sTacklesPercentageAverage { get; set; }
        public double sPenaltyShootOutPercentage { get; set; }
		public double sPenaltyShootOutPercentageAverage { get; set; }
        public double sPassingPercentage { get; set; }
		public double sPassingPercentageAverage { get; set; }
        public double sDuelsPercentage { get; set; }
		public double sDuelsPercentageAverage { get; set; }
        public double sGoalsPercentage { get; set; }
		public double sGoalsPercentageAverage { get; set; }
        public double sCrossesPercentage { get; set; }
		public double sCrossesPercentageAverage { get; set; }
        public string updated { get; set; }
        public string linkDetail { get; set; }
        public string linkDetailCompetition { get; set; }
        public string linkDetailTeam { get; set; }
        public string linkDetailLeague { get; set; }
        public string leagueName { get; set; }
        public string teamName { get; set; }
        public string teamNickname { get; set; }
        public string website { get; set; }
        public string teamCode { get; set; }
        public int clubId { get; set; }
        public string clubName { get; set; }
        public string competitionName { get; set; }
    }

    public class MatchPreview
    {
        public Match matchDetail { get; set; }
        public string competitionId { get; set; }
        public List<Head2head> head2head { get; set; }
        public List<List<LeaderDetails>> leaderData { get; set; }
        public List<MatchHistory> homematches { get; set; }
        public List<MatchHistory> awaymatches { get; set; }
        public FilterLists FilterLists { get; set; }
        public List<string> leadersFieldOrder { get; set; }
        public List<string> compareFieldOrder { get; set; }
        public List<ComparisonStat> comparisonStats { get; set; }
        public Common common { get; set; }
        public Config config { get; set; }
    }
}