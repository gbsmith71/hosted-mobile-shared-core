﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
namespace HostedMobile
{
    public class Teams
    {
		public List<Team> teams { get; set; }
		public string competitionId { get; set; }
		public FilterLists FilterLists { get; set; }
		public Common common { get; set; }
		public Config config { get; set; }
    }

    public class Team : ObservableObject
	{
		public int teamId { get; set; }
		public int leagueId { get; set; }
		public int clubId { get; set; }
		public string teamName { get; set; }
		public string teamNickname { get; set; }
		public string teamNameInternational { get; set; }
		public string teamNicknameInternational { get; set; }
		public string status { get; set; }
		public string teamCode { get; set; }
		public string address1 { get; set; }
		public string teamCodeInternational { get; set; }
		public string address2 { get; set; }
		public string address3 { get; set; }
		public string suburb { get; set; }
		public string state { get; set; }
		public string postalCode { get; set; }
		public string country { get; set; }
		public string countryCode { get; set; }
		public string countryCodeIOC { get; set; }
		public string phone { get; set; }
		public string fax { get; set; }
		public string email { get; set; }
		public string facebook { get; set; }
		public string website { get; set; }
		public string twitter { get; set; }
		public string keywords { get; set; }
		public string ticketURL { get; set; }
		public string internationalReference { get; set; }
		public string externalId { get; set; }
		public string updated { get; set; }
		public string colourPrimary { get; set; }
		public string colourSecondary { get; set; }
		public string linkDetail { get; set; }
		public string linkDetailLeague { get; set; }
		public string leagueName { get; set; }
		public string clubName { get; set; }
		public string linkDetailClub { get; set; }
		public Images images { get; set; }
		public int index { get; set; }
		public Color RowColor
		{
			get
			{
				if (index % 2 == 0)
					return (Color)App.Current.Resources["TableStripe"];
				else
					return Color.White;
			}
		}

	}

}
