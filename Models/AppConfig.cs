﻿using System;
namespace HostedMobile
{
    public class AppConfigColors
	{
		public string BarBackground { get; set; }
		public string BarText { get; set; }
		public string CompPickerBackground { get; set; }
		public string CompPickerText { get; set; }
		public string TableStripeBackground { get; set; }
		public string TableStripeText { get; set; }
	}

	public class AppConfig
	{
		public AppConfigColors colors { get; set; }
	}

    public class AppConfigObject
	{
		public AppConfig appConfig { get; set; }
	}
}
