﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
namespace HostedMobile
{
    public class Boxscore : ObservableObject
	{
		public int periodNumber { get; set; }
		public string periodType { get; set; }
		public int matchId { get; set; }
		public int personId { get; set; }
		public int teamId { get; set; }
		public int competitionId { get; set; }
		public int leagueId { get; set; }
		public string externalId { get; set; }
		public string shirtNumber { get; set; }
		public string playingPosition { get; set; }
		public int participated { get; set; }
		public string DNPReason { get; set; }
		public int isPlayer { get; set; }
		public int isTeamOfficial { get; set; }
		public int isStarter { get; set; }
		public string updated { get; set; }
		public int sPointsInThePaint { get; set; }
		public int sPointsInThePaintAttempted { get; set; }
		public int sPointsInThePaintMade { get; set; }
		public int sPointsSecondChance { get; set; }
		public int sPointsFastBreak { get; set; }
		public int sPoints { get; set; }
		public int sMVPVotes { get; set; }
		public int sPER { get; set; }
		public int sPlus { get; set; }
		public int sReboundsDefensive { get; set; }
		public int sReboundsOffensive { get; set; }
		public int sTurnovers { get; set; }
		public int sTwoPointersMade { get; set; }
		public int sTwoPointersAttempted { get; set; }
		public int sThreePointersMade { get; set; }
		public int sThreePointersAttempted { get; set; }
		public int sSecondChancePointsAttempted { get; set; }
		public int sSecondChancePointsMade { get; set; }
		public int sSteals { get; set; }
		public string sMinutes { get; set; }
		public int sMinus { get; set; }
		public int sFastBreakPointsMade { get; set; }
		public int sFieldGoalsAttempted { get; set; }
		public int sFieldGoalsMade { get; set; }
		public int sFastBreakPointsAttempted { get; set; }
		public int sEfficiencyCustom { get; set; }
		public int sAssistsDefensive { get; set; }
		public int sBlocks { get; set; }
		public int sBlocksReceived { get; set; }
		public int sFoulsCoachDisqualifying { get; set; }
		public int sFoulsCoachTechnical { get; set; }
		public int sFoulsUnsportsmanlike { get; set; }
		public int sFreeThrowsAttempted { get; set; }
		public int sFreeThrowsMade { get; set; }
		public int sFoulsTechnical { get; set; }
		public int sFoulsPersonal { get; set; }
		public int sFoulsDisqualifying { get; set; }
		public int sFoulsOffensive { get; set; }
		public int sFoulsOn { get; set; }
		public int sAssists { get; set; }
		public double sThreePointersPercentage { get; set; }
		public double sStealsPercentage { get; set; }
		public double sSecondChancePointsPercentage { get; set; }
		public double sTrueShootingAttempts { get; set; }
		public double sTurnoversPercentage { get; set; }
		public double sDefensiveRating { get; set; }
		public double sOffensiveRating { get; set; }
		public double sTwoPointersPercentage { get; set; }
		public int sReboundsTotal { get; set; }
		public double sReboundsOffensivePercentage { get; set; }
		public double sFieldGoalsEffectivePercentage { get; set; }
		public double sFastBreakPointsPercentage { get; set; }
		public double sEfficiencyGameScore { get; set; }
		public double sFieldGoalsPercentage { get; set; }
		public double sFreeThrowsPercentage { get; set; }
		public double sReboundsDefensivePercentage { get; set; }
		public double sPointsInThePaintPercentage { get; set; }
		public double sPlusMinusPoints { get; set; }
		public double sAssistsTurnoverRatio { get; set; }
		public double sTrueShootingPercentage { get; set; }
		public int sReboundsPercentage { get; set; }
		public int sPenaltyShootOutSave { get; set; }
		public string sPenaltyShootOutResults { get; set; }
		public int sPenaltyShotsAttempted { get; set; }
		public int sPenaltyShotsMade { get; set; }
		public string sRedCardsTimes { get; set; }
		public int sRedCards { get; set; }
		public int sPenaltyShootOutMade { get; set; }
		public int sPenaltyShootOutAttempted { get; set; }
		public string sOwnGoalsAgainstTimes { get; set; }
		public int sOwnGoalsAgainst { get; set; }
		public int sPasses { get; set; }
		public int sPassesCompleted { get; set; }
		public int sPassingDistanceCovered { get; set; }
		public int sPassesKey { get; set; }
		public int sShots { get; set; }
		public int sShotsBlocked { get; set; }
		public int sTacklesLost { get; set; }
		public int sTackles { get; set; }
		public int sTacklesWon { get; set; }
		public int sThrowIns { get; set; }
		public string sYellowCardsTimes { get; set; }
		public int sYellowCards { get; set; }
		public string sSubstitutionOnTime { get; set; }
		public string sSubstitutionOffTime { get; set; }
		public int sShotsOffTarget { get; set; }
		public int sShotsInsideBox { get; set; }
		public int sShotsOnTarget { get; set; }
		public int sShotsOnWoodwork { get; set; }
		public int sShotsSaved { get; set; }
		public int sOffsides { get; set; }
		public int sCrosses { get; set; }
		public int sCorners { get; set; }
		public int sCrossesCompleted { get; set; }
		public int sDuels { get; set; }
		public int sDuelsWon { get; set; }
		public int sDuelsLost { get; set; }
		public int sClearancesHeader { get; set; }
		public int sClearances { get; set; }
		public int sBallsRecovered { get; set; }
		public int sChancesCreated { get; set; }
		public int sClaimedCrosses { get; set; }
		public int sCleanSheets { get; set; }
		public int sClaimedSmother { get; set; }
		public int sEfficiency { get; set; }
		public int sGoalsPenaltyKick { get; set; }
		public int sGoalsLeftFoot { get; set; }
		public int sGoalsRightFoot { get; set; }
		public string sGoalsTimes { get; set; }
		public int sInterceptions { get; set; }
		public int sGoalsHeader { get; set; }
		public int sGoalsFreeKick { get; set; }
		public int sFoulsWon { get; set; }
		public int sFoulsCommitted { get; set; }
		public int sFreeKicks { get; set; }
		public int sGoals { get; set; }
		public int sGoalsConceded { get; set; }
		public double sPossessionsPercentage { get; set; }
		public int sShotsOnTargetPercentage { get; set; }
		public int sShotsOutsideBox { get; set; }
		public int sTacklesPercentage { get; set; }
		public int sPenaltyShotsPercentage { get; set; }
		public int sPenaltyShootOutPercentage { get; set; }
		public double sDuelsPercentage { get; set; }
		public double sGoalsPercentage { get; set; }
		public double sPassingPercentage { get; set; }
		public int sCrossesPercentage { get; set; }
		public string linkDetail { get; set; }
		public string linkDetailMatch { get; set; }
		public string linkDetailTeam { get; set; }
		public string linkDetailPerson { get; set; }
		public string linkDetailLeague { get; set; }
		public string leagueName { get; set; }
		public string teamName { get; set; }
		public string teamNameInternational { get; set; }
		public string personName { get; set; }
		public string firstName { get; set; }
		public string familyName { get; set; }
		public string TVName { get; set; }
		public string scoreboardName { get; set; }
		public string nickName { get; set; }
		public string internationalFirstName { get; set; }
		public string internationalFamilyName { get; set; }
		public string website { get; set; }
		public string internationalReference { get; set; }
		public string nationalityCodeIOC { get; set; }
		public string nationalityCode { get; set; }
		public string nationality { get; set; }
		public string linkDetailCompetitionPlayer { get; set; }
		public Images images { get; set; }
		public int index { get; set; }
		public Color RowColor
		{
			get
			{
				if (index % 2 == 0)
					return (Color)App.Current.Resources["TableStripe"];
				else
					return Color.White;
			}
		}
	}

    public class MatchBoxscore
	{
		public Match matchDetail { get; set; }
		public List<string> fieldOrder { get; set; }
		public List<Boxscore> boxscore_hometeam { get; set; }
		public List<Boxscore> boxscore_awayteam { get; set; }
		public List<Boxscore> boxscoreteam_hometeam { get; set; }
		public List<Boxscore> boxscoreteam_awayteam { get; set; }
		public string competitionId { get; set; }
		public FilterLists FilterLists { get; set; }
		public Common common { get; set; }
		public Config config { get; set; }
	}
}
