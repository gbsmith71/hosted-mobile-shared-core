﻿using System;
using System.Collections.Generic;
using HostedMobile.Core.ResX;
using Xamarin.Forms;

namespace HostedMobile
{
    public class Matches
	{
        public ObservableRangeCollection<string> dates { get; set; }
		public List<Match> matches { get; set; }
		public string competitionId { get; set; }
		public string dateFilter { get; set; }
		public string current_roundNumber { get; set; }
		public int current_poolNumber { get; set; }
		public string current_phaseName { get; set; }
		public string current_matchType { get; set; }
		public FilterLists FilterLists { get; set; }
        public List<PoolDetail> poolDetails { get; set; }
		public List<PhaseDetail> phases { get; set; }
		public List<MatchType> matchTypes { get; set; }
        public List<RoundDetail> roundDetails { get; set; }
		public Common common { get; set; }
		public Config config { get; set; }	}

	public class Match : ObservableObject
	{
		public int leagueId { get; set; }
		public int matchId { get; set; }
		public int competitionId { get; set; }
		public int venueId { get; set; }
		public int poolNumber { get; set; }
		public string roundNumber { get; set; }
		public string roundDescription { get; set; }
		public int matchNumber { get; set; }
		public string matchStatus { get; set; }
		public string matchName { get; set; }
		public string phaseName { get; set; }
		public int extraPeriodsUsed { get; set; }
		public string matchTime { get; set; }
		public string matchTimeUTC { get; set; }
		public DateTime shortDate
		{
			get
			{
				DateTime returnDate = Convert.ToDateTime(matchTime);
				return returnDate;
			}
		}
		public object enddate { get; set; }
		public string timeActual { get; set; }
		public string timeEndActual { get; set; }
		public int durationActual { get; set; }
		public int temperature { get; set; }
		public int attendance { get; set; }
		public int duration { get; set; }
		public string weather { get; set; }
		public string twitterHashtag { get; set; }
		public int liveStream { get; set; }
		public string matchType { get; set; }
		public string keywords { get; set; }
		public string ticketURL { get; set; }
		public string externalId { get; set; }
		public int nextMatchId { get; set; }
		public int placeIfWon { get; set; }
		public int placeIfLost { get; set; }
		public string updated { get; set; }
		public string linkDetail { get; set; }
		public string linkDetailLeague { get; set; }
		public string streamKey { get; set; }
		public Venue venue { get; set; }
		public string leagueName { get; set; }
		public string leagueNameInternational { get; set; }
		public string competitionName { get; set; }
		public string competitionNameInternational { get; set; }
		public List<Competitor> competitors { get; set; }
        public bool inOT { get; set; }
        public bool inShootOut { get; set; }
        public bool IsLive { get; set; }
        public bool IsPostponed { get; set; }
        public string postponedStatus { get; set; }
        public Color RowColor
		{
			get
			{
				if (IsLive  == true)
					return (Color)App.Current.Resources["LiveBackgroundColor"];
				else
					return Color.White;
			}
		}
	}

    public class Competitor : ObservableObject
    {
        public string competitorType { get; set; }
        public string competitorName { get; set; }
        public int competitorId { get; set; }
        public string linkDetailCompetitor { get; set; }
        public string scoreString { get; set; }
        public string scoreSecondaryString { get; set; }
        public string penaltyString { 
            get
            {
                if (scoreSecondaryString != "")
                {
                    return "(" + scoreSecondaryString + ")";
                }
                return "";
            }
        }
        public bool inPenalties { get; set; }
        public string completionStatus { get; set; }
        public int resultPlacing { get; set; }
        public int isDrawn { get; set; }
        public int isHomeCompetitor { get; set; }
        public int teamId { get; set; }
        public string teamName { get; set; }
        public string teamNameInternational { get; set; }
        public string teamNickname { get; set; }
        public string teamNicknameInternational { get; set; }
        public string teamCode { get; set; }
        public string teamCodeInternational { get; set; }
        public string website { get; set; }
        public string internationalReference { get; set; }
        public string externalId { get; set; }
        public Images images { get; set; }
        public int clubId { get; set; }
        public string clubName { get; set; }
        public string clubNameInternational { get; set; }
        public string linkDetailClub { get; set; }
        public bool current { get; set; }
        public List<Scorer> scorers { get; set; }
    }

	public class Venue
	{
		public int venueId { get; set; }
		public string venueName { get; set; }
		public string venueNameInternational { get; set; }
		public string venueNickname { get; set; }
		public string venueNicknameInternational { get; set; }
		public string surfaceName { get; set; }
		public string locationName { get; set; }
		public string website { get; set; }
		public string ticketURL { get; set; }
		public string externalId { get; set; }
		public string linkDetailVenue { get; set; }
	}

	public class RoundDetail : ObservableObject
	{
		public string number { get; set; }
		public string name { get; set; }
	}
    public class PoolDetail : ObservableObject
	{
		public string poolName { get; set; }
        public string internationalName { 
            get{
                if(poolName == "All"){
                    return AppResources.All;
                } else {
					return poolName;
				}
            }
        }
		public string poolId { get; set; }
		public bool current
		{
			get
			{
                if (Settings.CurrentPool == poolId)
				{
					return true;
				}
				else
				{
                    return false;
				}
			}
		}
	}
	public class PhaseDetail : ObservableObject
	{
		public string name { get; set; }
		public bool current
		{
			get
			{
				if (Settings.CurrentPhase == name)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}

	}
	public class MatchType : ObservableObject
	{
		public string matchType { get; set; }
		public bool current
		{
			get
			{
                if (Settings.CurrentMatchType == matchType)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}

	}
}
