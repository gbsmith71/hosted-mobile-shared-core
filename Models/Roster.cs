﻿using System;
using System.Collections.Generic;
using HostedMobile.Core.ResX;

using Xamarin.Forms;

namespace HostedMobile
{

	public class TeamDetail
	{
		public int teamId { get; set; }
		public int clubId { get; set; }
		public string teamName { get; set; }
		public string teamNickname { get; set; }
		public string teamNameInternational { get; set; }
		public string teamNicknameInternational { get; set; }
		public string status { get; set; }
		public string teamCode { get; set; }
		public string address1 { get; set; }
		public string teamCodeInternational { get; set; }
		public string address2 { get; set; }
		public string address3 { get; set; }
		public string suburb { get; set; }
		public string state { get; set; }
		public string postalCode { get; set; }
		public string country { get; set; }
		public string countryCode { get; set; }
		public string countryCodeIOC { get; set; }
		public string phone { get; set; }
		public string fax { get; set; }
		public string email { get; set; }
		public string facebook { get; set; }
		public string website { get; set; }
		public string twitter { get; set; }
		public string keywords { get; set; }
		public string ticketURL { get; set; }
		public string internationalReference { get; set; }
		public string externalId { get; set; }
		public string updated { get; set; }
		public string colourPrimary { get; set; }
		public string colourSecondary { get; set; }
		public string linkDetail { get; set; }
		public string linkDetailLeague { get; set; }
		public string leagueName { get; set; }
		public string clubName { get; set; }
		public string linkDetailClub { get; set; }
		public Images images { get; set; }
	}

	public class PlayerData
	{
		public int personId { get; set; }
		public int leagueId { get; set; }
		public string firstName { get; set; }
		public string familyName { get; set; }
        public string fullName
		{
			get
			{
                return firstName + " " + familyName;
			}
		}
		public string TVName { get; set; }
		public string scoreboardName { get; set; }
		public string internationalFirstName { get; set; }
		public string internationalFamilyName { get; set; }
		public string namePronunciation { get; set; }
		public string dob { get; set; }
		public string gender { get; set; }
		public string nickName { get; set; }
		public int height { get; set; }
		public int weight { get; set; }
		public string dominantHand { get; set; }
		public string dominantFoot { get; set; }
		public string internationalReference { get; set; }
		public string nationality { get; set; }
		public string nationalityCode { get; set; }
		public string nationalityCodeIOC { get; set; }
		public string facebook { get; set; }
		public string website { get; set; }
		public string twitter { get; set; }
		public string defaultShirtNumber { get; set; }
		public string defaultPlayingPosition { get; set; }
		public int primaryClubId { get; set; }
		public int primaryTeamId { get; set; }
		public string keywords { get; set; }
		public string externalId { get; set; }
		public string updated { get; set; }
		public string linkDetail { get; set; }
		public string linkDetailLeague { get; set; }
		public string linkDetailCompetitionPlayer { get; set; }
		//public CompetitionPlayer competition_player { get; set; }
		public Images images { get; set; }
		public string position { get; set; }
		public string positionTranslated { 
			get{
				if(GlobalVariables.Sport == "FOOTBALL"){
					if (position == "keeper"){
						position = "goalkeeper";
					}
					var positionString = "POSITION_" + GlobalVariables.Sport + "_" + position.ToUpper() + "_NAME";
					return AppResources.ResourceManager.GetString(positionString);
				} else {
					return position;
				}
			}
		}
		public string number { get; set; }
		public int index { get; set; }
		public Color RowColor
		{
			get
			{
				if (index % 2 == 0)
					return (Color)App.Current.Resources["TableStripe"];
				else
					return Color.White;
			}
		}

	}

    public class Roster
	{
		public TeamDetail teamDetail { get; set; }
		public List<PlayerData> playerData { get; set; }
		public List<string> fieldOrder { get; set; }
		//public FieldLabels fieldLabels { get; set; }
		public string competitionId { get; set; }
		public FilterLists FilterLists { get; set; }
		public Common common { get; set; }
		public Config config { get; set; }
	}
}
