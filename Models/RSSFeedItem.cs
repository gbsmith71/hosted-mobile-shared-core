﻿using System;
using System.Text.RegularExpressions;
using System.Net;

namespace HostedMobile
{
    public class RSSFeedItem : ObservableObject
	{
		public string Description { get; set; }

        private string content = "";
		public string Content { get
            {
                var decoded = WebUtility.HtmlDecode(content);

				decoded = Regex.Replace(decoded, "<[^>]*>", string.Empty);
				decoded = Regex.Replace(decoded, @"^\s*$\n", string.Empty, RegexOptions.Multiline);

                return decoded;
            }
            set{
                content = value; 
            } 
        }
		public string Link { get; set; }
		public string PublishDate { get; set; }
		public string Author { get; set; }
		public string AuthorEmail { get; set; }
		public int Id { get; set; }

		private string title;
		public string Title
		{
			get
			{
				return title;
			}
			set
			{
				title = value;
				var splitIndex = title.IndexOf(":", StringComparison.OrdinalIgnoreCase);
				if (splitIndex > -1)
				{
					Author = title.Substring(0, splitIndex).Trim();
					title = title.Substring(splitIndex + 1, title.Length - splitIndex - 1).Trim();
				}
			}
		}

		private string caption;

		public string Caption
		{
			get
			{
				if (!string.IsNullOrWhiteSpace(caption))
					return caption;

				//get rid of HTML tags
				caption = Regex.Replace(Description, "<[^>]*>", string.Empty);

				//get rid of multiple blank lines
				caption = Regex.Replace(caption, @"^\s*$\n", string.Empty, RegexOptions.Multiline);

				return caption;
			}
		}


		private string source;
		public string Source
		{
			get
			{
                source = "<iframe id='ytplayer' type='text/html' width='100%' height='360' src = 'https://www.youtube.com/embed/" + Link + "?autoplay=1&origin=http://geniussports.com' frameborder = '0' ></ iframe >";
                return source;
			}
		}
		private bool showImage = true;

		public bool ShowImage
		{
			get { return showImage; }
			set { showImage = value; }
		}

		private string image;

		public string Image
		{
			get { return image; }
			set
			{
				image = value;
				showImage = true;
			}

		}

	}
}