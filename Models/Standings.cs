﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
namespace HostedMobile
{
public class Standing : ObservableObject
	{
    	public int competitionId { get; set; }
    	public int competitorId { get; set; }
    	public int leagueId { get; set; }
    	public int poolNumber { get; set; }
    	public int roundNumber { get; set; }
    	public string roundDescription { get; set; }
    	public string phaseName { get; set; }
    	public int position { get; set; }
    	public int played { get; set; }
    	public int playedHome { get; set; }
    	public int playedAway { get; set; }
    	public int won { get; set; }
    	public int lost { get; set; }
    	public int drawn { get; set; }
    	public int byes { get; set; }
    	public int forfeitsReceived { get; set; }
    	public int forfeitsGiven { get; set; }
    	public int scoredFor { get; set; }
    	public int scoredAgainst { get; set; }
    	public int scoredForHome { get; set; }
    	public int scoredAgainstHome { get; set; }
    	public int scoredForAway { get; set; }
    	public int scoredAgainstAway { get; set; }
    	public double percentage { get; set; }
    	public int percentageWon { get; set; }
    	public int gamesBehind { get; set; }
    	public int standingPoints { get; set; }
    	public int pointsDiff { get; set; }
    	public int pointsDiffHome { get; set; }
    	public int pointsDiffAway { get; set; }
    	public int standingPointsHome { get; set; }
    	public int standingPointsAway { get; set; }
    	public int lowestScore { get; set; }
    	public int highestScore { get; set; }
    	public int bonusPoints { get; set; }
    	public int penaltyPoints { get; set; }
    	public int championshipPoints { get; set; }
    	public int byePoints { get; set; }
    	public int homeWins { get; set; }
    	public int homeLosses { get; set; }
    	public int awayWins { get; set; }
    	public int awayLosses { get; set; }
    	public int inConferenceWins { get; set; }
    	public int inDivisionWins { get; set; }
    	public int outConferenceWins { get; set; }
    	public int outDivisionWins { get; set; }
    	public int inConferenceLosses { get; set; }
    	public int inDivisionLosses { get; set; }
    	public int outConferenceLosses { get; set; }
    	public int outDivisionLosses { get; set; }
    	public int inPoolWins { get; set; }
    	public int inPoolLosses { get; set; }
    	public int inPhaseWins { get; set; }
    	public int inPhaseLosses { get; set; }
    	public int inPoolDraws { get; set; }
    	public int inPhaseDraws { get; set; }
    	public int outPoolWins { get; set; }
    	public int outPoolLosses { get; set; }
    	public int outPhaseWins { get; set; }
    	public int outPhaseLosses { get; set; }
    	public int outPoolDraws { get; set; }
    	public int outPhaseDraws { get; set; }
    	public int washouts { get; set; }
    	public object generatedDate { get; set; }
    	public int homeDraws { get; set; }
    	public int awayDraws { get; set; }
    	public int inConferenceDraws { get; set; }
    	public int inDivisionDraws { get; set; }
    	public int outConferenceDraws { get; set; }
    	public int outDivisionDraws { get; set; }
    	public string streak { get; set; }
    	public string streakHome { get; set; }
    	public string streakAway { get; set; }
    	public string last5 { get; set; }
    	public string last5Home { get; set; }
    	public string last5Away { get; set; }
    	public double forAgainstRatio { get; set; }
    	public double forAgainstRatioHome { get; set; }
    	public double forAgainstRatioAway { get; set; }
    	public int percentagePoints { get; set; }
    	public int securedFinalsSpot { get; set; }
    	public string matchType { get; set; }
    	public string keywords { get; set; }
    	public string externalId { get; set; }
    	public int liveStanding { get; set; }
    	public int liveInProgress { get; set; }
    	public string updated { get; set; }
    	public string linkDetail { get; set; }
    	public string linkDetailLeague { get; set; }
    	public string linkDetailCompetition { get; set; }
    	public int latest { get; set; }
    	public string competitionName { get; set; }
    	public string linkDetailTeam { get; set; }
    	public string teamName { get; set; }
    	public string teamNameInternational { get; set; }
    	public string teamNickname { get; set; }
    	public string teamNicknameInternational { get; set; }
    	public string teamCode { get; set; }
    	public string teamCodeInternational { get; set; }
    	public Images images { get; set; }
    	public int teamId { get; set; }
    	public string linkDetailClub { get; set; }
    	public string clubName { get; set; }
    	public int index { get; set; }
    	public Color RowColor
    		{
    			get
    			{
    				if (index % 2 == 0)
                        return (Color)App.Current.Resources["TableStripe"];
                    else
                        return Color.White;
    			}
    		}
    }

	public class StandingsObject
	{
		public List<string> fieldOrder { get; set; }
		public List<string> fieldOrder2 { get; set; }
		public List<Standing> standings { get; set; }
		public string competitionId { get; set; }
		public string current_roundNumber { get; set; }
		public int current_poolNumber { get; set; }
		public string current_phaseName { get; set; }
		public string current_matchType { get; set; }
		public FilterLists FilterLists { get; set; }
        public List<PhaseDetail> phases { get; set; }
		public List<PoolDetail> poolDetails { get; set; }
		public List<MatchType> matchTypes { get; set; }
		public List<RoundDetail> roundDetails { get; set; }
		public Common common { get; set; }
		public Config config { get; set; }
	}
}
