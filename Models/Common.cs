﻿using System;
using System.Collections;
using System.Collections.Generic;
using Xamarin.Forms;

namespace HostedMobile
{

    public class Common
	{
		public string userId { get; set; }
		public LeagueDetails LeagueDetails { get; set; }
		public CompDetails CompDetails { get; set; }
		public string schedule { get; set; }
		public string scheduleName { get; set; }
		public string standings { get; set; }
		public string standingsName { get; set; }
		public string teams { get; set; }
		public string teamsName { get; set; }
	}

    public class Competitions {
		public Competition competition { get; set; }
	}

    public class Competition
	{
		public string name { get; set; }
		public int leagueId { get; set; }
		public int competitionId { get; set; }
		public int competitionOrder { get; set; }
		public int current { get; set; }
		public int year { get; set; }
	}

    public class LeadersBlockContent : ObservableObject
	{
		public string Title { get; set; }
        public List<LeaderDetails> Leaders { get; set; }
	}

    public class League
	{
		public string name { get; set; }
		public int leagueId { get; set; }
	}

	public class FilterLists
	{
		public List<Competition> competitions { get; set; }
		public List<League> leagues { get; set; }
		public List<int> years { get; set; }
	}

    public class LeagueDetails
	{
		public int leagueId { get; set; }
		public string sport { get; set; }
		public string leagueName { get; set; }
		public string leagueAbbrev { get; set; }
		public string leagueNameInternational { get; set; }
		public string leagueAbbrevInternational { get; set; }
		public string address1 { get; set; }
		public string address2 { get; set; }
		public string address3 { get; set; }
		public string suburb { get; set; }
		public string state { get; set; }
		public string postalCode { get; set; }
		public string country { get; set; }
		public string countryCode { get; set; }
		public string countryCodeIOC { get; set; }
		public string phone { get; set; }
		public string fax { get; set; }
		public string email { get; set; }
		public string facebook { get; set; }
		public string website { get; set; }
		public string twitter { get; set; }
		public string timezone { get; set; }
		public string lga { get; set; }
		public int latitude { get; set; }
		public int longitude { get; set; }
		public string geographicRegion { get; set; }
		public string competitionStandard { get; set; }
		public string keywords { get; set; }
		public string externalId { get; set; }
		public int officialData { get; set; }
		public string externalModificationReference { get; set; }
		public string linkDetail { get; set; }
		public Images images { get; set; }
		public string logo { get; set; }
	}

	public class CompDetails
	{
		public int competitionId { get; set; }
		public int leagueId { get; set; }
		public string competitionName { get; set; }
		public string competitionAbbrev { get; set; }
		public string competitionNameInternational { get; set; }
		public string competitionAbbrevInternational { get; set; }
		public string season { get; set; }
		public string startDate { get; set; }
		public object endDate { get; set; }
		public int year { get; set; }
		public int numberCompetitors { get; set; }
		public int current { get; set; }
		public string gender { get; set; }
		public string ageGroup { get; set; }
		public string representation { get; set; }
		public string standard { get; set; }
		public int competitionOrder { get; set; }
		public int groupingOrder { get; set; }
		public string groupingKey { get; set; }
		public string grade { get; set; }
		public string competitorType { get; set; }
		public string includeInCareerStatistics { get; set; }
		public int numberOfPeriods { get; set; }
		public int periodLength { get; set; }
		public int extraTime { get; set; }
		public int extraTimeLength { get; set; }
		public int finalsPositions { get; set; }
		public string facebook { get; set; }
		public string website { get; set; }
		public string twitter { get; set; }
		public int standingConfigurationId { get; set; }
		public string ticketURL { get; set; }
		public string keywords { get; set; }
		public string externalId { get; set; }
		public string linkDetail { get; set; }
		public string linkDetailLeague { get; set; }
		public string leagueName { get; set; }
	}

    public class L1
	{
		public string size { get; set; }
		public int height { get; set; }
		public int width { get; set; }
		public int bytes { get; set; }
		public string url { get; set; }
	}

	public class M1
	{
		public string size { get; set; }
		public int height { get; set; }
		public int width { get; set; }
		public int bytes { get; set; }
		public string url { get; set; }
	}

	public class S1
	{
		public string size { get; set; }
		public int height { get; set; }
		public int width { get; set; }
		public int bytes { get; set; }
		public string url { get; set; }
	}

	public class T1
	{
		public string size { get; set; }
		public int height { get; set; }
		public int width { get; set; }
		public int bytes { get; set; }
		public string url { get; set; }
	}

	public class Logo
	{
		public L1 L1 { get; set; }
		public M1 M1 { get; set; }
		public S1 S1 { get; set; }
		public T1 T1 { get; set; }
	}

	public class Photo
	{
		public L1 L1 { get; set; }
		public M1 M1 { get; set; }
		public S1 S1 { get; set; }
		public T1 T1 { get; set; }
	}

    public class Images
	{
		public Logo logo { get; set; }
		public Photo photo { get; set; }
	}

    public class Config
	{
	}

	public class Rounditem
	{
		public string name { get; set; }
		public int id { get; set; }
	}

	public class Round
	{
		public int poolID { get; set; }
		public string name { get; set; }
		public List<Rounditem> rounditems { get; set; }
	}

	public class Pool
	{
		public int poolID { get; set; }
		public string value { get; set; }
	}

    public class PageButton : ObservableObject
	{
        private bool _current;
        public bool Current {
			get
			{
				return _current;
			}

			set
			{
				_current = value;
                OnPropertyChanged("Current");
			}
        }
		public string ButtonText { get; set; }
		public string Page { get; set; }
        public int IndexTab { get; set; }
	}

}
