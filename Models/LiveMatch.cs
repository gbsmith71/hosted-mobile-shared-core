﻿using Xamarin.Forms;
using System;
using System.Collections.Generic;

namespace HostedMobile
{
    public class LiveMatch
	{
		public string clock { get; set; }
		public int period { get; set; }
        public string periodType { get; set; }
		public int inOT { get; set; }
        public string timeAdded { get; set; }
		public Tm tm { get; set; }
		public Totallds totallds { get; set; }
        public Scorers scorers { get; set; }
        public List<LivePbp> pbp { get; set; }
	}

    public class Tm : ObservableObject
	{
		public LiveCompetitor a1 { get; set; }
		public LiveCompetitor b2 { get; set; }
	}

    public class Totallds : ObservableObject
	{
		public LiveLeadersList sBlocks { get; set; }
		public LiveLeadersList sSteals { get; set; }
		public LiveLeadersList sAssists { get; set; }
		public LiveLeadersList sReboundsTotal { get; set; }
        public LiveLeadersList sPoints { get; set; }
        public LiveLeadersList sGoals { get; set; }
        public LiveLeadersList sShotsOnTarget { get; set; }
        public LiveLeadersList sCorners { get; set; }
        public LiveLeadersList sYellowCards { get; set; }
        public LiveLeadersList sRedCards { get; set; }
        public LiveLeadersList sCrosses { get; set; }
	}

	public class LiveLeadersList
	{
		public TotalLeader a1 { get; set; }
		public TotalLeader b2 { get; set; }
		public TotalLeader c3 { get; set; }
		public TotalLeader d4 { get; set; }
		public TotalLeader e5 { get; set; }
	}

	public class TotalLeader
	{
		public string name { get; set; }
		public int tot { get; set; }
		public int tno { get; set; }
		public int pno { get; set; }
		public string teamName { get; set; }
		public string firstName { get; set; }
		public string familyName { get; set; }
		public string internationalFirstName { get; set; }
		public string internationalFamilyName { get; set; }
		public string firstNameInitial { get; set; }
		public string familyNameInitial { get; set; }
		public string internationalFirstNameInitial { get; set; }
		public string internationalFamilyNameInitial { get; set; }
		public string scoreboardName { get; set; }
		public string photoT { get; set; }
		public string photoS { get; set; }
	}

	public class LiveLeadersBlockContent : ObservableObject
	{
		public string Title { get; set; }
		public List<TotalLeader> Leaders { get; set; }
	}


	public class LiveCompetitor : ObservableObject
	{
		public string name { get; set; }
		public string nameInternational { get; set; }
		public string shortName { get; set; }
		public string shortNameInternational { get; set; }
		public string code { get; set; }
		public string codeInternational { get; set; }
		public int score { get; set; }
		public int full_score { get; set; }
        public List<Scorer> scorers { get; set; }
		public int p1_score { get; set; }
		public int p2_score { get; set; }
		public int p3_score { get; set; }
		public int p4_score { get; set; }
		public int ot_score { get; set; }
		public int fouls { get; set; }
		public int timeouts { get; set; }
		public int tot_sMinutes { get; set; }
		public int tot_eff_1 { get; set; }
		public int tot_eff_2 { get; set; }
		public double tot_eff_3 { get; set; }
		public double tot_eff_4 { get; set; }
		public int tot_eff_5 { get; set; }
		public int tot_eff_6 { get; set; }
		public int tot_eff_7 { get; set; }
		public int tot_sFieldGoalsMade { get; set; }
		public int tot_sFieldGoalsAttempted { get; set; }
		public int tot_sFieldGoalsPercentage { get; set; }
		public int tot_sThreePointersMade { get; set; }
		public int tot_sThreePointersAttempted { get; set; }
		public int tot_sThreePointersPercentage { get; set; }
		public int tot_sTwoPointersMade { get; set; }
		public int tot_sTwoPointersAttempted { get; set; }
		public int tot_sTwoPointersPercentage { get; set; }
		public int tot_sFreeThrowsMade { get; set; }
		public int tot_sFreeThrowsAttempted { get; set; }
		public int tot_sFreeThrowsPercentage { get; set; }
		public int tot_sReboundsDefensive { get; set; }
		public int tot_sReboundsOffensive { get; set; }
		public int tot_sReboundsTotal { get; set; }
		public int tot_sAssists { get; set; }
		public int tot_sTurnovers { get; set; }
		public int tot_sSteals { get; set; }
		public int tot_sBlocks { get; set; }
		public int tot_sBlocksReceived { get; set; }
		public int tot_sFoulsPersonal { get; set; }
		public int tot_sFoulsOn { get; set; }
		public int tot_sPoints { get; set; }
		public int tot_sPointsFromTurnovers { get; set; }
		public int tot_sPointsSecondChance { get; set; }
		public int tot_sPointsFastBreak { get; set; }
		public int tot_sBenchPoints { get; set; }
		public int tot_sPointsInThePaint { get; set; }
		public double tot_sTimeLeading { get; set; }
		public int tot_sBiggestLead { get; set; }
		public int tot_sBiggestScoringRun { get; set; }
		public int tot_sLeadChanges { get; set; }
		public int tot_sTimesScoresLevel { get; set; }
		public int tot_sFoulsTeam { get; set; }
		public int tot_sReboundsTeam { get; set; }
		public int tot_sReboundsTeamDefensive { get; set; }
		public int tot_sReboundsTeamOffensive { get; set; }
		public int tot_sEfficiencyCustom { get; set; }
		public int tot_sTurnoversTeam { get; set; }
        public int tot_sGoals { get; set; }
        public int tot_sShots { get; set; }
        public int tot_sShotsOnTarget { get; set; }
        public int tot_sShotsOnTargetPercentage { get; set; }
        public int tot_sShotsInsideBox { get; set; }
        public int tot_sCorners { get; set; }
        public int tot_sOffsides { get; set; }
        public int tot_sTackles { get; set; }
        public int tot_sTacklesPercentage { get; set; }
        public int tot_sYellowCards { get; set; }
        public int tot_sRedCards { get; set; }
        public int tot_sPasses { get; set; }
        public int tot_sPassesCompleted { get; set; }
        public int tot_sInterceptions { get; set; }
        public int tot_sCrosses { get; set; }
        public int tot_sCrossesPercentage { get; set; }
	}

    public class OtherMatches : ObservableObject
	{
		public int leagueId { get; set; }
		public int competitionId { get; set; }
		public int matchId { get; set; }
		public string matchStatus { get; set; }
		public string matchTime { get; set; }
		public string matchTimeUTC { get; set; }
		public int live { get; set; }
		public string homename { get; set; }
		public string homecode { get; set; }
		public string homenameInternational { get; set; }
		public int hometeamId { get; set; }
		public object homescore { get; set; }
		public string homelogo { get; set; }
		public string awayname { get; set; }
		public string awaycode { get; set; }
		public string awaynameInternational { get; set; }
		public int awayteamId { get; set; }
		public object awayscore { get; set; }
		public string awaylogo { get; set; }
		public string clock { get; set; }
		public int period { get; set; }
		public string periodType { get; set; }
	}
    public class Scorers : ObservableObject
    {
        public ObservableRangeCollection<Scorer> Team1 { get; set; }
        public ObservableRangeCollection<Scorer> Team2 { get; set; }
    }
    public class Scorer : ObservableObject
    {
        public int tno { get; set; }
        public string pno { get; set; }
        public string player { get; set; }
        public string shirtNumber { get; set; }
        public string firstName { get; set; }
        public string familyName { get; set; }
        public string internationalFirstName { get; set; }
        public string internationalFamilyName { get; set; }
        public string firstNameInitial { get; set; }
        public string familyNameInitial { get; set; }
        public string internationalFirstNameInitial { get; set; }
        public string internationalFamilyNameInitial { get; set; }
        public string scoreboardName { get; set; }
        public List<ScoreTime> times { get; set; }
        public string summary { get; set; }
        public string displaySummary
        {
            get
            {
                String summaryTime;
                summaryTime = summary.Replace(" +", "'+").Replace(" ", "', ").Replace("'+", "' +");
                if (summaryTime.Contains("+") == false){
                    summaryTime = summaryTime + "'";
                }
                return summaryTime;
            }
        }
        public ImageSource type
        {
            get
            {
                if (pno.Contains("OWN") == true)
                    return ImageSource.FromFile("owngoal");
                else
                    return ImageSource.FromFile("goal");
            }
        }
    }
    public class ScoreTime
    {
        public string gt { get; set; }
        public int per { get; set; }
        public string perType { get; set; }
        public string ta { get; set; }
    }
    public class LivePbp
    {
        public string gt { get; set; }
        public int s1 { get; set; }
        public int s2 { get; set; }
        public int lead { get; set; }
        public int tno { get; set; }
        public int period { get; set; }
        public string periodType { get; set; }
        public int pno { get; set; }
        public string player { get; set; }
        public int success { get; set; }
        public string actionType { get; set; }
        public string qualifier { get; set; }
        public string subType { get; set; }
        public int scoring { get; set; }
        public string shirtNumber { get; set; }
        public string ta { get; set; }
        public int priority { get; set; }
    }
}
