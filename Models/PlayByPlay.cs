﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HostedMobile.Core.ResX;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HostedMobile
{

    public class Playbyplaydata : ObservableObject
	{
		public int actionNumber { get; set; }
		public int matchId { get; set; }
		public int leagueId { get; set; }
		public string clock { get; set; }
		public string shotClock { get; set; }
		public string timeActual { get; set; }
		public int period { get; set; }
		public string periodType { get; set; }
		public int teamId { get; set; }
        public bool isHome { get; set; }
		public bool isAway { get; set; }
		public bool isScore { get; set; }
		public int personId { get; set; }
		public string actionType { get; set; }
		public string subType { get; set; }
		public string qualifiers { get; set; }
		public string value { get; set; }
		public int success { get; set; }
		public string shirtNumber { get; set; }
		public int officialId { get; set; }
		public string playersTeam1 { get; set; }
		public string playersTeam2 { get; set; }
		public string score1 { get; set; }
		public string score2 { get; set; }
		public string teamName { get; set; }
		public string teamLogo { get; set; }
		public string teamCode { get; set; }
		public string firstName { get; set; }
        public string familyName { get; set; }
        public string injuryTime { get; set; }
        [JsonIgnore]
        public ContentView actionContent {
            get {
                var content = new ContentView();
                switch (actionType)
				{
					case "game":
					case "period":
						var gameoutput = new PlayGame();
                        gameoutput.TitleText = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_ACTION_" + actionType.ToUpper() + "_" + subType.ToUpper());
                        gameoutput.ScoreText = score1 + " - " + score2;
                        content.Content = gameoutput;
						return content;
                    default:
						var output = new PlayBasic();
                        string subtypeString = "";
						string[] scoreTypes = { "2pt", "3pt", "freethrow","goal"};
                        string successString = "";
                        if (scoreTypes.Contains(actionType)){
                            if (success == 1){
                                successString = " " + AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_shotMade");
                                isScore = true;
								output.ScoreText = score1 + "-" + score2;
							} else {
								successString = " " + AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_shotMissed"); 
							}
                        }
                        if(actionType == "shootoutkick"){
                            if(success == 1){
                                subType = "success";
                            } else {
                                subType = "missed";
                            }
                        }
                        if (subType != ""){
                            subtypeString = "_" + subType.ToUpper();
                        }
                        String translationString;
                        translationString = GlobalVariables.Sport + "_ACTION_" + actionType.ToUpper() + subtypeString;
                        if (firstName != null)
                        {
                            output.TitleText = shirtNumber + ". " + firstName + " " + familyName + ", " + AppResources.ResourceManager.GetString(translationString) + successString;
                        } else {
                            output.TitleText = AppResources.ResourceManager.GetString(translationString) + successString;
                        }
                        output.IsHome = isHome;
                        output.IsAway = isAway;
                        String periodString;
                        if (periodType == "OVERTIME" || periodType == "EXTRATIME"){
                            periodString = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_OVERTIME_ABBREV");
                        } else if (periodType == "PENALTYSHOOTOUT"){
                            periodString = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_SHOOTOUT_ABBREV");                            
                        } else {
							periodString = AppResources.ResourceManager.GetString(GlobalVariables.Sport + "_PERIOD_ABBREV_" + period.ToString());
						}
                        output.PeriodText = periodString;
                        if (GlobalVariables.Sport == "FOOTBALL")
                        {
                            if (injuryTime != null)
                            {
                                output.ClockText = injuryTime;
                            }
                            else
                            {
                                string[] clockParts = clock.Split(":".ToCharArray());
                                output.ClockText = (Int32.Parse(clockParts[0])+1) + "'";
                            }
                            if(periodType == "PENALTYSHOOTOUT"){
                                output.ClockText = "";  
                            }
						}
                        else
                        {
							output.ClockText = clock;
						}
                        output.TeamLogo = teamLogo;
						content.Content = output;
						return content;
				}

            }
        }
    }
    public class PeriodButton  : ObservableObject {
        public String title { get; set; }
		public List<Playbyplaydata> periodData { get; set; }
		public bool current { get; set; }
		public bool IsVisible { get; set; }
	}

	public class PlayByPlay
    {
		public Match matchDetail { get; set; }
		public List<Playbyplaydata> playbyplaydata { get; set; }
		public FilterLists FilterLists { get; set; }
		public Common common { get; set; }
		public Config config { get; set; }
	}
}
