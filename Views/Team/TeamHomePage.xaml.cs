﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HostedMobile
{
    public partial class TeamHomePage : TabbedPage
    {
        public TeamHomePage(Team item)
        {
            InitializeComponent();
            this.Children.Add(new TeamRosterPage(item) {
                Title = "Roster", 
                Icon = "tab_teams.png" });

		}
    }
}
