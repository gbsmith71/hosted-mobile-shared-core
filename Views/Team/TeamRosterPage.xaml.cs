﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using HostedMobile.Core.ResX;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
namespace HostedMobile
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TeamRosterPage : ContentPage
    {
        TeamRosterViewModel viewModel;

        public TeamRosterPage(Team team)
        {
            InitializeComponent();
			BindingContext = viewModel = new TeamRosterViewModel(team);
            viewModel.LoadRosterCommand.Execute(team.teamId);
            LoadView(team.teamId.ToString());
        }

  //      async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
		//{
   //         var item = args.SelectedItem as PlayerData;
			//if (item == null)
				//return;

			// await Navigation.PushAsync(new TeamHomePage(new TeamRosterViewModel(item)));

			// Manually deselect item
			// ItemsListView.SelectedItem = null;
		//}

		protected override void OnAppearing()
		{
			base.OnAppearing();

            //viewModel.LoadRosterCommand.Execute(null);
			// LoadView();
		}

		public async Task LoadView(String teamId)
		{
			var hs = new HostedDataService();
            var rosterObject = await hs.GetRoster(teamId, true);
            App.rosterFieldsOrder = rosterObject.fieldOrder.ToArray();

			ContentView Header = new CustomStandingsHeader();
			ListHeader.Children.Clear();
			ListHeader.Children.Add(Header);
            RosterRepeater2.ItemTemplate = new DataTemplate(typeof(CustomStandingsCell));
		}

		public class CustomStandingsCell : ViewCell
		{
			public CustomStandingsCell()
			{
				Label[] dynamicLabel = new Label[50];

				var horizontalLayout = new StackLayout()
				{
					BackgroundColor = Color.White,
					Padding = 0,
					Spacing = 10,
					Orientation = StackOrientation.Horizontal,
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    VerticalOptions = LayoutOptions.Start,
					HeightRequest = 50,
				};
				horizontalLayout.SetBinding(StackLayout.BackgroundColorProperty, new Binding("RowColor"));

				String[] Fields = new string[50];
                Fields = App.rosterFieldsOrder;

				string[] excludedFields = { "playerName", "shirtNumber" };

				if (Fields.Count() > 0)
				{
					int i = 0;
					foreach (var currentItem in Fields)
					{
						String fieldName;
						if(currentItem == "position"){
							fieldName = "positionTranslated";
						} else {
							fieldName = currentItem;	
						}
						if (!excludedFields.Contains(fieldName))
                        {
                            dynamicLabel[i] = new Label()
                            {
                                WidthRequest = 120,
                                HeightRequest = 50,
                                FontSize = 13,
                                Margin = 0,
                                VerticalTextAlignment = TextAlignment.Center,
                                HorizontalTextAlignment = TextAlignment.Center,
                                LineBreakMode = LineBreakMode.TailTruncation,
                                VerticalOptions = LayoutOptions.Center,
                                HorizontalOptions = LayoutOptions.Start,
                            };
                            switch (Xamarin.Forms.Device.RuntimePlatform)
                            {
                                case Xamarin.Forms.Device.iOS:
                                    dynamicLabel[i].FontFamily = "AvenirNext-Regular";
                                    break;
                                case Xamarin.Forms.Device.Android:

                                    break;
                            }

							dynamicLabel[i].SetBinding(Label.TextProperty, new Binding(fieldName));
                            horizontalLayout.Children.Add(dynamicLabel[i]);  // adding the label
                        }
                        i++;
					}
				}

				View = horizontalLayout;
			}
		}

		public class CustomStandingsHeader : ContentView
		{
			public CustomStandingsHeader()
			{
				Label[] dynamicLabel = new Label[50];

				var horizontalLayout = new StackLayout()
				{
                    BackgroundColor = Color.Transparent,
					Orientation = StackOrientation.Horizontal,
					HorizontalOptions = LayoutOptions.Fill,
					Spacing = 10,
					Padding = 0,
				};

				String[] Fields = new string[50];
				Fields = App.rosterFieldsOrder;
				string[] excludedFields = { "playerName", "shirtNumber" };

                if (Fields.Count() > 0)
				{
					int i = 0;
					foreach (var currentItem in Fields)
					{
                        if (!excludedFields.Contains(currentItem))
                        {

                            dynamicLabel[i] = new Label()
                            {
                                Text = AppResources.ResourceManager.GetString("ROSTER_" + currentItem),
                                WidthRequest = 120,
                                FontSize = 12,
                                VerticalTextAlignment = TextAlignment.Center,
                                HorizontalTextAlignment = TextAlignment.Center,
                                LineBreakMode = LineBreakMode.TailTruncation,
								BackgroundColor = (Color)App.Current.Resources["TableHeaderBackground"],
								TextColor = (Color)App.Current.Resources["TableHeaderText"],
								Style = (Style)App.Current.Resources["HeadingText"]
							};
                            switch (Xamarin.Forms.Device.RuntimePlatform)
                            {
                                case Xamarin.Forms.Device.iOS:

                                    break;
                                case Xamarin.Forms.Device.Android:

                                    break;
                            }

                            horizontalLayout.Children.Add(dynamicLabel[i]);  // adding the label
                        }
                        i++;
					}
				}
				Content = horizontalLayout;
			}

		}

	}
}
