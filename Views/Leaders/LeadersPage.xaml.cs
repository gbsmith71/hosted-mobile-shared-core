﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HostedMobile
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LeadersPage : ContentPage
    {

        LeadersViewModel viewModel;

        public LeadersPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new LeadersViewModel();

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            viewModel.LoadLeadersCommand.Execute(null);

        }
    }
}
