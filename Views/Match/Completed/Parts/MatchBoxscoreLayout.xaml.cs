﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using HostedMobile.Core.ResX;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HostedMobile
{
    public partial class MatchBoxscoreLayout : StackLayout
    {
        public MatchBoxscoreLayout()
        {
            InitializeComponent();
			LoadView();

		}
		public async Task LoadView()
		{
			var hs = new HostedDataService();
            var boxscoreObject = await hs.GetMatchBoxscore(App.currentMatch.matchId.ToString(), true);
			App.boxscoreFieldsOrder = boxscoreObject.fieldOrder.ToArray();

			ContentView Header = new CustomStandingsHeader();
			ListHeader.Children.Clear();
			ListHeader.Children.Add(Header);
			BoxscoreRepeater2.ItemTemplate = new DataTemplate(typeof(CustomStandingsCell));
			BoxscoreRepeater3.ItemTemplate = new DataTemplate(typeof(CustomStandingsCell));
		}

		public class CustomStandingsCell : ViewCell
		{
			public CustomStandingsCell()
			{
				Label[] dynamicLabel = new Label[50];

				var horizontalLayout = new StackLayout()
				{
					BackgroundColor = Color.White,
					Padding = 0,
					Spacing = 10,
					Orientation = StackOrientation.Horizontal,
					HorizontalOptions = LayoutOptions.StartAndExpand,
					VerticalOptions = LayoutOptions.Start,
					HeightRequest = 50,
				};
				horizontalLayout.SetBinding(BackgroundColorProperty, new Binding("RowColor"));

				String[] Fields = new string[50];
				Fields = App.boxscoreFieldsOrder;
				string[] excludedFields = { "playerName", "shirtNumber" };

				if (Fields.Count() > 0)
				{
					int i = 0;
					foreach (var currentItem in Fields)
					{
						if (!excludedFields.Contains(currentItem))
						{
							dynamicLabel[i] = new Label()
							{
								WidthRequest = 45,
								HeightRequest = 50,
								FontSize = 15,
								Margin = 0,
								VerticalTextAlignment = TextAlignment.Center,
								HorizontalTextAlignment = TextAlignment.Center,
								LineBreakMode = LineBreakMode.TailTruncation,
								VerticalOptions = LayoutOptions.Center,
								HorizontalOptions = LayoutOptions.Start,
							};
							switch (Xamarin.Forms.Device.RuntimePlatform)
							{
								case Xamarin.Forms.Device.iOS:
									dynamicLabel[i].FontFamily = "AvenirNext-Regular ";
									break;
								case Xamarin.Forms.Device.Android:

									break;
							}
							if (currentItem.Contains("Percent"))
							{
								dynamicLabel[i].SetBinding(Label.TextProperty, new Binding(currentItem, stringFormat: "{0:P1}"));
							}
							else
							{
								dynamicLabel[i].SetBinding(Label.TextProperty, new Binding(currentItem));
							}
							horizontalLayout.Children.Add(dynamicLabel[i]);  // adding the label
						}
						i++;
					}
				}

				View = horizontalLayout;
			}
		}

		public class CustomStandingsHeader : ContentView
		{
			public CustomStandingsHeader()
			{
				Label[] dynamicLabel = new Label[50];

				var horizontalLayout = new StackLayout()
				{
					BackgroundColor = Color.Transparent,
					Orientation = StackOrientation.Horizontal,
					HorizontalOptions = LayoutOptions.Fill,
					Spacing = 10,
					Padding = 0,
				};

				String[] Fields = new string[50];
				Fields = App.boxscoreFieldsOrder;
				string[] excludedFields = { "playerName", "shirtNumber" };

				if (Fields.Count() > 0)
				{
					int i = 0;
					foreach (var currentItem in Fields)
					{
						if (!excludedFields.Contains(currentItem))
						{

							dynamicLabel[i] = new Label()
							{
								Text = AppResources.ResourceManager.GetString("STAT_PERSONMATCH_" + GlobalVariables.Sport + "_" + currentItem + "_ABBREV"),
								WidthRequest = 45,
								FontSize = 12,
								VerticalTextAlignment = TextAlignment.Center,
								HorizontalTextAlignment = TextAlignment.Center,
								LineBreakMode = LineBreakMode.TailTruncation,
								BackgroundColor = (Color)App.Current.Resources["TableHeaderBackground"],
								TextColor = (Color)App.Current.Resources["TableHeaderText"],
								Style = (Style)App.Current.Resources["HeadingText"]
							};
							switch (Xamarin.Forms.Device.RuntimePlatform)
							{
								case Xamarin.Forms.Device.iOS:

									break;
								case Xamarin.Forms.Device.Android:

									break;
							}

							horizontalLayout.Children.Add(dynamicLabel[i]);  // adding the label
						}
						i++;
					}
				}
				Content = horizontalLayout;
			}

		}

	}
}
