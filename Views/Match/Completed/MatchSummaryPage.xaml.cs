﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using HostedMobile.Core.ResX;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HostedMobile
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MatchSummaryPage : ContentPage
    {
        MatchCompleteViewModel viewModel;
		Match _matchDetails;

		public MatchSummaryPage(Match matchDetails)
        {
			InitializeComponent();
			_matchDetails = matchDetails;
			BindingContext = viewModel = new MatchCompleteViewModel(matchDetails);

			PageCarousel.PositionSelected += (o, s) =>
			{
				viewModel.PagePosition = PageCarousel.Position;
				viewModel.UpdatePagePosition.Execute(null);

			};
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

            viewModel.LoadMatchSummaryCommand.Execute(_matchDetails);

		}

		async void CloseMatch(object sender, EventArgs args)
		{
			await Navigation.PopModalAsync();
		}

	}
}
