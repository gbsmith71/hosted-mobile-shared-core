﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HostedMobile.Core.Views.Match.Completed
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MatchShotChartPage : ContentPage
    {
        public MatchShotChartPage()
        {
            InitializeComponent();
        }
    }
}
