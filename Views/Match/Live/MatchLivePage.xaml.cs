﻿using System;
using System.Threading;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HostedMobile
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MatchLivePage : ContentPage
    {
		MatchLiveViewModel viewModel;
		Match _matchDetails;

        public MatchLivePage(Match matchDetails)
        {
            InitializeComponent();
			_matchDetails = matchDetails;
            BindingContext = viewModel = new MatchLiveViewModel(matchDetails);

            var reloadInterval = TimeSpan.FromSeconds(25);

			Device.StartTimer(reloadInterval, () =>
			{
				viewModel.LoadMatchLiveCommand.Execute(_matchDetails);
				return true;
			});
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
            viewModel.LoadMatchLiveCommand.Execute(_matchDetails);
		}

		async void CloseMatch(object sender, EventArgs args)
		{
			await Navigation.PopModalAsync();
		}
	}
}
