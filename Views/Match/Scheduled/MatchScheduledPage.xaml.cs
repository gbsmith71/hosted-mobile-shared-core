﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HostedMobile
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MatchScheduledPage : ContentPage
    {
        MatchScheduledViewModel viewModel;
        Match _matchDetails;

		public MatchScheduledPage(Match matchDetails)
        {
            InitializeComponent();
            _matchDetails = matchDetails;
            BindingContext = viewModel = new MatchScheduledViewModel(matchDetails);
        }

		protected override void OnAppearing()
		{
			base.OnAppearing();

            viewModel.LoadMatchPreviewCommand.Execute(_matchDetails);

		}

		async void CloseMatch(object sender, EventArgs args)
		{
            await Navigation.PopModalAsync();
		}

	}
}
