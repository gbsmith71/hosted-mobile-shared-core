﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HostedMobile
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TeamsPage : ContentPage
	{
		TeamsViewModel viewModel;

		public TeamsPage()
		{
			InitializeComponent();
			BindingContext = viewModel = new TeamsViewModel(this.Navigation);
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			viewModel.LoadTeamsCommand.Execute(null);

		}
	}
}
