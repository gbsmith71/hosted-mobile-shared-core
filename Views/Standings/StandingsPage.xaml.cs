﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using HostedMobile.Core.ResX;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HostedMobile
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StandingsPage : ContentPage
	{
		StandingsViewModel viewModel;

		public StandingsPage()
		{
			InitializeComponent();
			BindingContext = viewModel = new StandingsViewModel();
		}

		//async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
		//{
		//	var item = args.SelectedItem as Team;
		//	if (item == null)
		//		return;

  //          await Navigation.PushAsync(new TeamRosterPage(new TeamRosterViewModel(item)));

		//	ItemsListView.SelectedItem = null;
		//}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			viewModel.LoadStandingsCommand.Execute(null);
            LoadView();
		}

        public async Task LoadView()
        {
			var hs = new HostedDataService();
			var standingsObject = await hs.GetStandings(true);

			App.standingsFieldsOrder = standingsObject.fieldOrder.ToArray();

			ContentView Header = new CustomStandingsHeader();
            ListHeader.Children.Clear();
			ListHeader.Children.Add(Header);
			ItemsListView.ItemTemplate = new DataTemplate(typeof(CustomStandingsCell));

		}

		public class CustomStandingsCell : ViewCell
		{
			public CustomStandingsCell()
			{
				Label[] dynamicLabel = new Label[50];

				var horizontalLayout = new StackLayout()
				{
					BackgroundColor = Color.White,
					Padding = 5,
					Spacing = 15,
					Orientation = StackOrientation.Horizontal,
					HorizontalOptions = LayoutOptions.Fill,
                    HeightRequest = 30,

				};
				horizontalLayout.SetBinding(StackLayout.BackgroundColorProperty, new Binding("RowColor"));

				String[] Fields = new string[50];
				Fields = App.standingsFieldsOrder;
				if (Fields.Count() > 0)
				{
					int i = 0;
					foreach (var currentItem in Fields)
					{
						dynamicLabel[i] = new Label()
						{
							WidthRequest = 40,
							FontSize = 13,
							VerticalTextAlignment = TextAlignment.Center,
							HorizontalTextAlignment = TextAlignment.Center,
						};
						switch (Xamarin.Forms.Device.RuntimePlatform)
						{
							case Xamarin.Forms.Device.iOS:
								dynamicLabel[i].FontFamily = "AvenirNext-Regular";
								break;
							case Xamarin.Forms.Device.Android:

								break;
						}

						dynamicLabel[i].SetBinding(Label.TextProperty, new Binding(currentItem));
						horizontalLayout.Children.Add(dynamicLabel[i]);  // adding the label
						i++;
					}
				}

				View = horizontalLayout;
			}
		}

        public class CustomStandingsHeader : ContentView
		{
			public CustomStandingsHeader()
			{
				Label[] dynamicLabel = new Label[50];

				var horizontalLayout = new StackLayout()
				{
                    BackgroundColor = Color.Transparent,
					Orientation = StackOrientation.Horizontal,
					HorizontalOptions = LayoutOptions.Fill,
					Spacing = 15,
					Padding = 3,
				};

				String[] Fields = new string[50];
				Fields = App.standingsFieldsOrder;
				if (Fields.Count() > 0)
				{
					int i = 0;
					foreach (var currentItem in Fields)
					{
						dynamicLabel[i] = new Label()
						{
                            Text = AppResources.ResourceManager.GetString("STANDINGS_" + currentItem + "_ABBREV"),
							WidthRequest = 40,
							FontSize = 12,
							VerticalTextAlignment = TextAlignment.Center,
							HorizontalTextAlignment = TextAlignment.Center,
							LineBreakMode = LineBreakMode.TailTruncation,
                            BackgroundColor = (Color)App.Current.Resources["TableHeaderBackground"],
							TextColor = (Color)App.Current.Resources["TableHeaderText"],
                            Style = (Style)App.Current.Resources["HeadingText"]
						};
						switch (Xamarin.Forms.Device.RuntimePlatform)
						{
							case Xamarin.Forms.Device.iOS:
								
                                break;
							case Xamarin.Forms.Device.Android:

								break;
						}

						horizontalLayout.Children.Add(dynamicLabel[i]);  // adding the label
						i++;
					}
				}
				Content = horizontalLayout;
			}

		}


	}
}