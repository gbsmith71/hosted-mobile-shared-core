﻿using System;
using HostedMobile.Core.ResX;
using Xamarin.Forms;

namespace HostedMobile
{
    public class StartPage : ExtendedTab
    {
        public StartPage()
        {
            if (GlobalVariables.ShowSchedule){
				var schedulePage = new NavigationPage(new SchedulePage());
				
                if (Device.RuntimePlatform == Device.iOS)
				{
					schedulePage.Icon = "tab_schedule.png";
				}
                schedulePage.Title = AppResources.ResourceManager.GetString("Schedule");

				Children.Add(schedulePage);
			}
			if (GlobalVariables.ShowStandings)
			{
                var standingsPage = new NavigationPage(new StandingsPage());
				if (Device.RuntimePlatform == Device.iOS)
				{
					standingsPage.Icon = "tab_standings.png";
				}
                standingsPage.Title = AppResources.ResourceManager.GetString("Standings");

				Children.Add(standingsPage);
			}
            if (GlobalVariables.ShowTeams)
			{
				var teamsPage = new NavigationPage(new TeamsPage());
				if (Device.RuntimePlatform == Device.iOS)
				{
					teamsPage.Icon = "tab_teams.png";
				}
                teamsPage.Title = AppResources.ResourceManager.GetString("Teams");

				Children.Add(teamsPage);
			}
            if (GlobalVariables.ShowLeaders)
            {
                var leadersPage = new NavigationPage(new LeadersPage());
                if (Device.RuntimePlatform == Device.iOS)
                {
                    leadersPage.Icon = "tab_leaders.png";
                }
                leadersPage.Title = AppResources.ResourceManager.GetString("Leaders");

                Children.Add(leadersPage);
            }
            if (GlobalVariables.ShowNews)
			{
				var newsPage = new NavigationPage(new NewsListPage());
				if (Device.RuntimePlatform == Device.iOS)
				{
					newsPage.Icon = "tab_news.png";
				}
                newsPage.Title = AppResources.ResourceManager.GetString("News");

				Children.Add(newsPage);
			}
            //this.BarTextColor = Color.DarkMagenta;
            //this.BarBackgroundColor = Color.DeepPink;
		}
    }
}