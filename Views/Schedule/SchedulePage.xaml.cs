﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HostedMobile
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SchedulePage : ContentPage
    {
        ScheduleViewModel viewModel;

        public SchedulePage()
        {
            InitializeComponent();
            BindingContext = viewModel = new ScheduleViewModel();
            var reloadInterval = TimeSpan.FromSeconds(25);

            Device.StartTimer(reloadInterval, () =>
            {
                viewModel.LoadMatchesCommand.Execute(null);
                return true;
            });

        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var matchDetails = args.SelectedItem as Match;
            if (matchDetails == null)
                return;
            App.currentMatch = matchDetails;

            if (matchDetails.matchStatus == "SCHEDULED")
            {
                await Navigation.PushModalAsync(new MatchScheduledPage(matchDetails), false);
            }
            else if (matchDetails.matchStatus == "IN_PROGRESS")
            {
                await Navigation.PushModalAsync(new MatchLivePage(matchDetails), false);
            }
            else if (matchDetails.matchStatus == "ABANDONED" || matchDetails.matchStatus == "CANCELLED" || matchDetails.matchStatus == "POSTPONED")
            {
                // Do Nothing
            }
            else
            {
                await Navigation.PushModalAsync(new MatchSummaryPage(matchDetails), false);
            }

            ItemsListView.SelectedItem = null;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            viewModel.LoadMatchesCommand.Execute(null);

        }

    }
}
