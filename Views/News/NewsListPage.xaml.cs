﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
namespace HostedMobile
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewsListPage : ContentPage
    {
        FeedViewModel viewModel;

        public NewsListPage()
        {
            InitializeComponent();

			BindingContext = viewModel = new FeedViewModel();

            NewsCarousel.PositionSelected += (o, s) =>
            {
                viewModel.PagePosition = NewsCarousel.Position;
				viewModel.UpdatePagePosition.Execute(null);

			};
		}

        protected override void OnAppearing()
		{
			base.OnAppearing();
			viewModel.LoadNewsCommand.Execute(null);
		}

	}
}
