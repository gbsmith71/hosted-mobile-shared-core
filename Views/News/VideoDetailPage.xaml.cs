﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HostedMobile
{
    public partial class VideoDetailPage : ContentPage
    {
        public VideoDetailPage(RSSFeedItem _NewsItem)
        {
            InitializeComponent();
            BindingContext = _NewsItem;

        }

	}
}
