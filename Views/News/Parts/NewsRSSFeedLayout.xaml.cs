﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HostedMobile
{
    public partial class NewsRSSFeedLayout : StackLayout
    {
        public NewsRSSFeedLayout()
        {
            InitializeComponent();
        }

		async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
		{
			var item = args.SelectedItem as RSSFeedItem;
			if (item == null)
				return;

			await Navigation.PushAsync(new NewsDetailPage(item));

			NewsListView.SelectedItem = null;
		}
	}
}
