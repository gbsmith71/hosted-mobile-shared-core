﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HostedMobile
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewsDetailPage : ContentPage
    {
        public NewsDetailPage(RSSFeedItem _NewsItem)
        {
            InitializeComponent();
            BindingContext = _NewsItem;
        }
    }
}
