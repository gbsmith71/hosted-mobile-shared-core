﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace HostedMobile
{
	/// <summary>
	/// This is the Settings static class that can be used in your Core solution or in any
	/// of your client applications. All settings are laid out the same exact way with getters
	/// and setters. 
	/// </summary>
	public static class Settings
	{
		private static ISettings AppSettings
		{
			get
			{
				return CrossSettings.Current;
			}
		}

		#region Setting Constants
		const string CurrentCompKey = "currentcomp";
		private static readonly string CurrentCompDefault = string.Empty;

		const string CurrentRoundKey = "currentround";
		private static readonly string CurrentRoundDefault = string.Empty;

		const string CurrentRoundNameKey = "currentroundname";
        private static readonly string CurrentRoundNameDefault = string.Empty;

		const string CurrentDateKey = "currentdate";
        private static readonly string CurrentDateDefault = string.Empty;

		const string CurrentPhaseKey = "currentphase";
		private static readonly string CurrentPhaseDefault = string.Empty;

		const string CurrentMatchTypeKey = "currentmatchtype";
		private static readonly string CurrentMatchTypeDefault = string.Empty;

		const string CurrentPoolKey = "currentpool";
		private static readonly string CurrentPoolDefault = string.Empty;

		const string BarBackgroundKey = "barbackground";
		private static readonly string BarBackgroundDefault = "#000000";

		const string BarTextKey = "bartext";
		private static readonly string BarTextDefault = "#ffffff";
        #endregion


		public static string CurrentComp
		{
			get
			{
				return AppSettings.GetValueOrDefault(CurrentCompKey, CurrentCompDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(CurrentCompKey, value);
			}
		}

		public static string CurrentRound
		{
			get
			{
				return AppSettings.GetValueOrDefault(CurrentRoundKey, CurrentRoundDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(CurrentRoundKey, value);
			}
		}

		public static string CurrentRoundName
        {
            get
            {
                return AppSettings.GetValueOrDefault(CurrentRoundNameKey, CurrentRoundNameDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(CurrentRoundNameKey, value);
            }
        }

		public static string CurrentDate
        {
            get
            {
                return AppSettings.GetValueOrDefault(CurrentDateKey, CurrentDateDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(CurrentDateKey, value);
            }
        }

		public static string CurrentPhase
		{
			get
			{
				return AppSettings.GetValueOrDefault(CurrentPhaseKey, CurrentPhaseDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(CurrentPhaseKey, value);
			}
		}

		public static string CurrentPool
		{
			get
			{
				return AppSettings.GetValueOrDefault(CurrentPoolKey, CurrentPoolDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(CurrentPoolKey, value);
			}
		}

		public static string CurrentMatchType
		{
			get
			{
				return AppSettings.GetValueOrDefault(CurrentMatchTypeKey, CurrentMatchTypeDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(CurrentMatchTypeKey, value);
			}
		}

		public static string BarBackground
		{
			get
			{
                return AppSettings.GetValueOrDefault(BarBackgroundKey, BarBackgroundDefault);
			}
			set
			{
                AppSettings.AddOrUpdateValue(BarBackgroundKey, value);
			}
		}

		public static string BarText
		{
			get
			{
                return AppSettings.GetValueOrDefault(BarTextKey, BarTextDefault);
			}
			set
			{
                AppSettings.AddOrUpdateValue(BarTextKey, value);
			}
		}

    }
}
