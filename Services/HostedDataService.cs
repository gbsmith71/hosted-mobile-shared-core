﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Akavache;
using System.Reactive.Linq;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;

namespace HostedMobile
{
    public class HostedDataService
    {
        HttpClient client;
		HttpClient liveClient;
		JsonSerializerSettings jsonHandler;

		public HostedDataService()
        {
			client = new HttpClient();
            string compString = "";
            if (Settings.CurrentComp.Length != 0){
                compString = $"competition/{Settings.CurrentComp}/";
            }

            client.BaseAddress = new Uri($"{GlobalVariables.BackendUrl}{GlobalVariables.ClientCode}/{compString}");

			jsonHandler = new JsonSerializerSettings
			{
				NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore,
				Error = (sender, args) =>
				{
					args.ErrorContext.Handled = true;

				}
			};
		}
		public async Task<StandingsObject> GetStandings(bool forceRefresh = false)
		{
			return await BlobCache.UserAccount.GetOrFetchObject<StandingsObject>("competition_standings_" + Settings.CurrentComp + "_" + Settings.CurrentPool + "_" + Settings.CurrentPhase + "_" + Settings.CurrentRound + "_" + Settings.CurrentMatchType,
				async () => await ReturnStandings(forceRefresh),
				DateTimeOffset.Now.AddMinutes(10));
		}


		public async Task<StandingsObject> ReturnStandings(bool forceRefresh = false)
		{
            var standingsObject = new StandingsObject();

			if (forceRefresh && CrossConnectivity.Current.IsConnected)
			{
				string roundString = "";
				roundString = $"&phaseName={Settings.CurrentPhase}&poolNumber={Settings.CurrentPool}";

 				var standingsjson = await client.GetStringAsync($"standings?json=1{roundString}");
                standingsObject = await Task.Run(() => JsonConvert.DeserializeObject<StandingsObject>(standingsjson, this.jsonHandler));
            }
			
            return standingsObject;

		}

        public async Task<Roster> GetRoster(string teamId, bool forceRefresh = false)
		{
			return await BlobCache.UserAccount.GetOrFetchObject<Roster>("team_" + teamId + "_roster",
				async () => await ReturnRoster(teamId, forceRefresh),
				DateTimeOffset.Now.AddMinutes(10));
		}


		public async Task<Roster> ReturnRoster(string teamId, bool forceRefresh = false)
		{
			var rosterObject = new Roster();

			if (forceRefresh && CrossConnectivity.Current.IsConnected)
			{
				var rosterjson = await client.GetStringAsync($"team/{teamId}/roster?json=1");

                rosterObject = await Task.Run(() => JsonConvert.DeserializeObject<Roster>(rosterjson, this.jsonHandler));
			}
			return rosterObject;

		}

        public async Task<Matches> GetMatches(bool forceRefresh = false)
		{
            return await BlobCache.UserAccount.GetOrFetchObject<Matches>("competition_matches_" + Settings.CurrentComp + "_" + Settings.CurrentPool + "_" + Settings.CurrentPhase + "_" + Settings.CurrentRound + "_" + Settings.CurrentMatchType + "_" + Settings.CurrentDate,
				async () => await ReturnMatches(forceRefresh),
				DateTimeOffset.Now.AddMinutes(60));
		}

		public async Task<Matches> ReturnMatches(bool forceRefresh = false)
		{
			var matchesObject = new Matches();

            if (forceRefresh && CrossConnectivity.Current.IsConnected)
			{
				string roundString = "";
                roundString = $"&roundNumber={Settings.CurrentRound}&phaseName={Settings.CurrentPhase}&matchType={Settings.CurrentMatchType}&poolNumber={Settings.CurrentPool}";
                if(Settings.CurrentDate != ""){
                    roundString = $"&dateFilter={Settings.CurrentDate}";
                }
                var json = await client.GetStringAsync($"schedule?json=1{roundString}");
                matchesObject = await Task.Run(() => JsonConvert.DeserializeObject<Matches>(json, this.jsonHandler));

			}

			return matchesObject;
		}

        public async Task<ObservableRangeCollection<OtherMatches>> GetOtherMatches(bool forceRefresh = false)
		{
			return await BlobCache.UserAccount.GetOrFetchObject<ObservableRangeCollection<OtherMatches>>("other_matches_" + Settings.CurrentComp ,
				async () => await ReturnOtherMatches(forceRefresh),
				DateTimeOffset.Now.AddMinutes(.1));
		}

		public async Task<ObservableRangeCollection<OtherMatches>> ReturnOtherMatches(bool forceRefresh = false)
		{
			var otherMatchesObject = new ObservableRangeCollection<OtherMatches>();

			if (forceRefresh && CrossConnectivity.Current.IsConnected)
			{
				liveClient = new HttpClient();
				liveClient.BaseAddress = new Uri($"{GlobalVariables.LiveUrl}");
				liveClient.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
                var ConnectionString = $"competition/{Settings.CurrentComp}.json";
				try
				{
					var json = await liveClient.GetStringAsync(ConnectionString);
					otherMatchesObject = await Task.Run(() => JsonConvert.DeserializeObject<ObservableRangeCollection<OtherMatches>>(json, this.jsonHandler));
				}
				catch (Exception e) 
                {
					if (e.Message == "404 (Not Found)"){
						otherMatchesObject = null;
					}
                }

			}

            return otherMatchesObject;
		}

        public async Task<MatchPreview> GetMatchPreview(String matchId, bool forceRefresh = false)
		{
			return await BlobCache.UserAccount.GetOrFetchObject<MatchPreview>("match_preview_" + matchId,
				async () => await ReturnMatchPreview(matchId, forceRefresh),
				DateTimeOffset.Now.AddMinutes(60));
		}

		public async Task<MatchPreview> ReturnMatchPreview(String matchId, bool forceRefresh = false)
		{
			var matchesObject = new MatchPreview();

			if (forceRefresh && CrossConnectivity.Current.IsConnected)
			{
                var json = await client.GetStringAsync($"match/{matchId}/preview?json=1");
				matchesObject = await Task.Run(() => JsonConvert.DeserializeObject<MatchPreview>(json, this.jsonHandler));
			}
            return matchesObject;
		}

		public async Task<MatchSummary> GetMatchSummary(String matchId, bool forceRefresh = false)
		{
			return await BlobCache.UserAccount.GetOrFetchObject<MatchSummary>("match_summary_" + matchId,
				async () => await ReturnMatchSummary(matchId, forceRefresh),
				DateTimeOffset.Now.AddMinutes(60));
		}

        public async Task<MatchSummary> ReturnMatchSummary(String matchId, bool forceRefresh = false)
		{
			var matchesObject = new MatchSummary();

			if (forceRefresh && CrossConnectivity.Current.IsConnected)
			{
				var json = await client.GetStringAsync($"match/{matchId}/summary?json=1");
				matchesObject = await Task.Run(() => JsonConvert.DeserializeObject<MatchSummary>(json, this.jsonHandler));
			}
			return matchesObject;
		}

		public async Task<MatchBoxscore> GetMatchBoxscore(String matchId, bool forceRefresh = false)
		{
			return await BlobCache.UserAccount.GetOrFetchObject<MatchBoxscore>("match_boxscore_" + matchId,
				async () => await ReturnMatchBoxscore(matchId, forceRefresh),
				DateTimeOffset.Now.AddMinutes(60));
		}

		public async Task<MatchBoxscore> ReturnMatchBoxscore(String matchId, bool forceRefresh = false)
		{
			var matchesObject = new MatchBoxscore();

			if (forceRefresh && CrossConnectivity.Current.IsConnected)
			{
				var json = await client.GetStringAsync($"match/{matchId}/boxscore?json=1");
				matchesObject = await Task.Run(() => JsonConvert.DeserializeObject<MatchBoxscore>(json, this.jsonHandler));
			}
			return matchesObject;
		}

        public async Task<PlayByPlay> GetMatchPlays(String matchId, bool forceRefresh = false)
		{
			return await BlobCache.UserAccount.GetOrFetchObject<PlayByPlay>("match_plays_" + matchId,
				async () => await ReturnMatchPlays(matchId, forceRefresh),
				DateTimeOffset.Now.AddMinutes(60));
		}

		public async Task<PlayByPlay> ReturnMatchPlays(String matchId, bool forceRefresh = false)
		{
			var matchesObject = new PlayByPlay();

			if (forceRefresh && CrossConnectivity.Current.IsConnected)
			{
				var json = await client.GetStringAsync($"match/{matchId}/playbyplay?json=1");
				matchesObject = await Task.Run(() => JsonConvert.DeserializeObject<PlayByPlay>(json, this.jsonHandler));
			}
			return matchesObject;
		}

        public async Task<Teams> GetTeams(bool forceRefresh = false)
		{
            return await BlobCache.UserAccount.GetOrFetchObject<Teams>("teamslist_" + Settings.CurrentComp,
				async () => await ReturnTeams(forceRefresh),
				DateTimeOffset.Now.AddMinutes(600));
		}

        public async Task<Teams> ReturnTeams(bool forceRefresh = false)
        {
            var teamObject = new Teams();

            if (forceRefresh && CrossConnectivity.Current.IsConnected)
            {
                var json = await client.GetStringAsync($"teams?json=1");
                teamObject = await Task.Run(() => JsonConvert.DeserializeObject<Teams>(json, this.jsonHandler));
                Settings.CurrentComp = teamObject.competitionId;
            }

            return teamObject;
        }

        public async Task<CompLeaders> GetCompLeaders(bool forceRefresh = false)
        {
            return await BlobCache.UserAccount.GetOrFetchObject<CompLeaders>("compLeaders" + Settings.CurrentComp,
                async () => await ReturnCompLeaders(forceRefresh),
                DateTimeOffset.Now.AddMinutes(60));
        }

        public async Task<CompLeaders> ReturnCompLeaders(bool forceRefresh = false)
        {
            var compLeadersObject = new CompLeaders();

            if (forceRefresh && CrossConnectivity.Current.IsConnected)
            {
                var json = await client.GetStringAsync($"leaders?json=1");
                compLeadersObject = await Task.Run(() => JsonConvert.DeserializeObject<CompLeaders>(json, this.jsonHandler));
                Settings.CurrentComp = compLeadersObject.competitionId;
            }

            return compLeadersObject;
        }

        public async Task<LiveMatch> GetLiveMatch(string matchId)
		{
			return await BlobCache.UserAccount.GetOrFetchObject<LiveMatch>("match_live_" + matchId,
				async () => await ReturnLiveMatch(matchId),
				DateTimeOffset.Now.AddSeconds(45));
		}

		public async Task<LiveMatch> ReturnLiveMatch(string matchId)
		{
			var liveMatchObject = new LiveMatch();

			if (CrossConnectivity.Current.IsConnected)
			{
				liveClient = new HttpClient();
				liveClient.BaseAddress = new Uri($"{GlobalVariables.LiveUrl}");
				liveClient.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");

				var ConnectionString = $"{matchId}/data.json";
				var json = await liveClient.GetStringAsync(ConnectionString);
                json = json.Replace("\"1\":[", "\"team1\":[").Replace("\"2\":[", "\"team2\":[");
                json = json.Replace("\"1\"", "\"a1\"").Replace("\"2\"", "\"b2\"").Replace("\"3\"", "\"c3\"").Replace("\"4\"", "\"d4\"").Replace("\"5\"", "\"e5\"");
				liveMatchObject = await Task.Run(() => JsonConvert.DeserializeObject<LiveMatch>(json, this.jsonHandler));
			}

			return liveMatchObject;
		}


		public List<string> GetFilterComps(FilterLists filter)
		{
			var complist = new List<string>();

            foreach (Competition comp in filter.competitions)
			{
                complist.Add(comp.name);
			}
			return complist;
		}

        public int GetFilterCompsSelected(FilterLists filter, string compID)
		{
			var selected = new int();
            int i = 0;
			foreach (Competition comp in filter.competitions)
			{
                if(int.Parse(compID) == comp.competitionId){
                    selected = i;
                }
                i++;
			}
			return selected;
		}
	}
}
