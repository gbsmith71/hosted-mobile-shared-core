﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Input;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HostedMobile
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LeadersBlock : StackLayout
    {
		public static readonly BindableProperty TitleTextProperty = BindableProperty.Create(
												 propertyName: "TitleText",
												 returnType: typeof(string),
												 declaringType: typeof(LeadersBlock),
												 defaultValue: "",
												 defaultBindingMode: BindingMode.TwoWay,
												 propertyChanged: TitleTextPropertyChanged);

		public string TitleText
		{
			get { return base.GetValue(TitleTextProperty).ToString(); }
			set { base.SetValue(TitleTextProperty, value); }
		}

        private static void TitleTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (LeadersBlock)bindable;
            var newText = "";
            if (newValue != null)
            {
                newText = newValue.ToString();
            } else {
				newText = "Unknown";
            }
            control.BlockTitle.Text = newText;
		}

		public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create(
														propertyName: "ItemSource",
														returnType: typeof(ICollection),
														declaringType: typeof(LeadersBlock),
                                                        defaultValue: null,
														defaultBindingMode: BindingMode.TwoWay,
														propertyChanged: ItemSourcePropertyChanged);
        
		public ICollection ItemsSource
		{
			get { return (ICollection)GetValue(ItemsSourceProperty); }
			set { base.SetValue(ItemsSourceProperty, value); }
		}

		private static void ItemSourcePropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (LeadersBlock)bindable;
            control.LeadersRepeaterView.ItemsSource = (ICollection)newValue;
		}

		public LeadersBlock()
        {
            InitializeComponent();
        }
    }
}
