﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HostedMobile
{
	public class CustomPicker : Picker
	{
		public static BindableProperty IconImageProperty = BindableProperty.Create(nameof(IconImage), typeof(string), typeof(CustomPicker), "dropdown");

		public string IconImage
		{
			get
			{
                return (string)GetValue(IconImageProperty);
			}
			set
			{
                SetValue(IconImageProperty, value);
			}
		}
        public static BindableProperty ThemeProperty = BindableProperty.Create(nameof(Theme), typeof(string), typeof(CustomPicker), "light");

        public string Theme
        {
            get
            {
                return (string)GetValue(ThemeProperty);
            }
            set
            {
                SetValue(ThemeProperty, value);
            }
        }
	}
}