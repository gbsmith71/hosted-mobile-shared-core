﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HostedMobile
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SelectorTab : StackLayout
	{
		public static readonly BindableProperty TitleTextProperty = BindableProperty.Create(
														 propertyName: "TitleText",
														 returnType: typeof(string),
														 declaringType: typeof(SelectorTab),
														 defaultValue: "",
														 defaultBindingMode: BindingMode.TwoWay,
														 propertyChanged: TitleTextPropertyChanged);

		public string TitleText
		{
			get { return base.GetValue(TitleTextProperty).ToString(); }
			set { base.SetValue(TitleTextProperty, value); }
		}

		private static void TitleTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (SelectorTab)bindable;
			control.title.Text = newValue.ToString();
		}

		public static readonly BindableProperty TextColorProperty = BindableProperty.Create(
														propertyName: "TextColor",
														returnType: typeof(Color),
														declaringType: typeof(SelectorTab),
														defaultValue: Color.Default,
														defaultBindingMode: BindingMode.TwoWay,
														propertyChanged: TextColorPropertyChanged);

		public Color TextColor
		{
			get { return (Color)GetValue(TextColorProperty); }
			set { base.SetValue(TextColorProperty, value); }
		}

		private static void TextColorPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (SelectorTab)bindable;
			control.title.TextColor = (Color)newValue;
			control.TextColor = (Color)newValue;
		}

		public static readonly BindableProperty FocusTextColorProperty = BindableProperty.Create(
														propertyName: "FocusTextColor",
														returnType: typeof(Color),
														declaringType: typeof(SelectorTab),
														defaultValue: Color.Default,
														defaultBindingMode: BindingMode.TwoWay,
														propertyChanged: FocusTextColorPropertyChanged);

		public Color FocusTextColor
		{
			get { return (Color)GetValue(FocusTextColorProperty); }
			set { base.SetValue(FocusTextColorProperty, value); }
		}

		private static void FocusTextColorPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (SelectorTab)bindable;
			//control.title.TextColor = (Color)newValue;
			//control.TextColor = (Color)newValue;
		}

		public static readonly BindableProperty FocusColorProperty = BindableProperty.Create(
														propertyName: "FocusColor",
														returnType: typeof(Color),
														declaringType: typeof(SelectorTab),
														defaultValue: Color.Default,
														defaultBindingMode: BindingMode.TwoWay,
														propertyChanged: FocusTextColorPropertyChanged);

		public Color FocusColor
		{
			get { return (Color)GetValue(FocusColorProperty); }
			set { base.SetValue(FocusColorProperty, value); }
		}

		private static void FocusColorPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (SelectorTab)bindable;
			//control.title.TextColor = (Color)newValue;
			//control.TextColor = (Color)newValue;
		}

        public static readonly BindableProperty BGColorProperty = BindableProperty.Create(
														propertyName: "BGColor",
														returnType: typeof(Color),
														declaringType: typeof(SelectorTab),
														defaultValue: Color.Default,
														defaultBindingMode: BindingMode.TwoWay,
														propertyChanged: BackgroundColorPropertyChanged);

		public Color BGColor
		{
			get { return (Color)GetValue(BGColorProperty); }
			set { base.SetValue(BGColorProperty, value); }
		}

		private static void BackgroundColorPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (SelectorTab)bindable;
			control.BackgroundColor = (Color)newValue;
			control.BGColor = (Color)newValue;
            control.arrow.TextColor = Color.Transparent;
        }

		public static readonly BindableProperty IsActiveProperty = BindableProperty.Create(
														propertyName: "IsActive",
														returnType: typeof(Boolean),
														declaringType: typeof(SelectorTab),
														defaultValue: false,
														defaultBindingMode: BindingMode.TwoWay,
														propertyChanged: IsActivePropertyChanged);

		public bool IsActive
		{
			get { return (Boolean)GetValue(IsActiveProperty); }
			set { base.SetValue(IsActiveProperty, value); }
		}

		private static void IsActivePropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (SelectorTab)bindable;
            if ((bool)newValue == true ){
                control.arrow.TextColor = control.FocusColor;
                control.title.TextColor = control.FocusTextColor;
                control.title.FontAttributes = FontAttributes.Bold;
				control.BackgroundColor = control.FocusColor;
			} else {
                control.arrow.TextColor = Color.Transparent;
                control.title.TextColor = control.TextColor;
                control.title.FontAttributes = FontAttributes.None;
				control.BackgroundColor = control.BGColor;
			}
		}

        public SelectorTab()
		{
			InitializeComponent();
		}

	}
}