﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HostedMobile
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PlayGame : StackLayout
	{
		public static readonly BindableProperty TitleTextProperty = BindableProperty.Create(
			propertyName: "TitleText", returnType: typeof(string),
			declaringType: typeof(PlayGame), defaultValue: "",
			defaultBindingMode: BindingMode.TwoWay,
			propertyChanged: TitleTextPropertyChanged);

		public string TitleText
		{
			get { return base.GetValue(TitleTextProperty).ToString(); }
			set { base.SetValue(TitleTextProperty, value); }
		}

		private static void TitleTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (PlayGame)bindable;
			control.title.Text = newValue.ToString();
		}

		public static readonly BindableProperty ScoreTextProperty = BindableProperty.Create(
			propertyName: "ScoreText", returnType: typeof(string),
            declaringType: typeof(PlayGame), defaultValue: "",
            defaultBindingMode: BindingMode.TwoWay,
            propertyChanged: ScoreTextPropertyChanged);

		public string ScoreText
		{
			get { return base.GetValue(ScoreTextProperty).ToString(); }
			set { base.SetValue(ScoreTextProperty, value); }
		}

		private static void ScoreTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (PlayGame)bindable;
			control.score.Text = newValue.ToString();
		}

		public PlayGame()
		{
			InitializeComponent();
		}


	}
}
