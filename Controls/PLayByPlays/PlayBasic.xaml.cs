﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HostedMobile
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PlayBasic : StackLayout
	{
		public static readonly BindableProperty TitleTextProperty = BindableProperty.Create(
		    propertyName: "TitleText", returnType: typeof(string),
		    declaringType: typeof(PlayBasic),defaultValue: "",
			defaultBindingMode: BindingMode.TwoWay,
			propertyChanged: TitleTextPropertyChanged);

		public string TitleText
		{
			get { return base.GetValue(TitleTextProperty).ToString(); }
			set { base.SetValue(TitleTextProperty, value); }
		}

		private static void TitleTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (PlayBasic)bindable;
			control.title.Text = newValue.ToString();
		}

		public static readonly BindableProperty TeamLogoProperty = BindableProperty.Create(
			propertyName: "TitleText", returnType: typeof(string),
			declaringType: typeof(PlayBasic), defaultValue: "",
			defaultBindingMode: BindingMode.TwoWay,
			propertyChanged: TeamLogoPropertyChanged);

		public string TeamLogo
		{
			get { return base.GetValue(TeamLogoProperty).ToString(); }
			set { base.SetValue(TeamLogoProperty, value); }
		}

		private static void TeamLogoPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (PlayBasic)bindable;
            if(newValue == null){
                newValue = ""; 
            }
            control.teamLogo.Source = newValue.ToString();
		}

		public static readonly BindableProperty PeriodTextProperty = BindableProperty.Create(
			propertyName: "TitleText", returnType: typeof(string),
			declaringType: typeof(PlayBasic), defaultValue: "",
			defaultBindingMode: BindingMode.TwoWay,
			propertyChanged: PeriodTextPropertyChanged);

		public string PeriodText
		{
			get { return base.GetValue(PeriodTextProperty).ToString(); }
			set { base.SetValue(PeriodTextProperty, value); }
		}

		private static void PeriodTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (PlayBasic)bindable;
            control.period.Text = newValue.ToString();
		}

		public static readonly BindableProperty ClockTextProperty = BindableProperty.Create(
			propertyName: "TitleText", returnType: typeof(string),
			declaringType: typeof(PlayBasic), defaultValue: "",
			defaultBindingMode: BindingMode.TwoWay,
			propertyChanged: ClockTextPropertyChanged);

		public string ClockText
		{
			get { return base.GetValue(ClockTextProperty).ToString(); }
			set { base.SetValue(ClockTextProperty, value); }
		}

		private static void ClockTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (PlayBasic)bindable;
			control.clock.Text = newValue.ToString();
		}

        public static readonly BindableProperty ScoreTextProperty = BindableProperty.Create(
			propertyName: "ScoreText", returnType: typeof(string),
			declaringType: typeof(PlayBasic), defaultValue: "",
			defaultBindingMode: BindingMode.TwoWay,
			propertyChanged: ScoreTextPropertyChanged);

		public string ScoreText
		{
			get { return base.GetValue(ScoreTextProperty).ToString(); }
			set { base.SetValue(ScoreTextProperty, value); }
		}

		private static void ScoreTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (PlayBasic)bindable;
            control.scoreLabel.Text = newValue.ToString();
            if (control.scoreLabel.Text.Length > 1)
            {
                control.scoreLabel.IsVisible = true;
                control.BackgroundColor = (Color)App.Current.Resources["PlayByPlayScoreBackground"];
            }
		}

		public static readonly BindableProperty IsHomeProperty = BindableProperty.Create(
			propertyName: "IsHome", returnType: typeof(Boolean),
			declaringType: typeof(PlayBasic), defaultValue: false,
			defaultBindingMode: BindingMode.TwoWay,
			propertyChanged: IsHomePropertyChanged);

		public bool IsHome
		{
			get { return (Boolean)GetValue(IsHomeProperty); }
			set { base.SetValue(IsHomeProperty, value); }
		}

		private static void IsHomePropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (PlayBasic)bindable;
            control.homeBar.IsVisible = (bool)newValue;
			control.notAwayBar.IsVisible = (bool)newValue;
		}

		public static readonly BindableProperty IsAwayProperty = BindableProperty.Create(
			propertyName: "IsAway", returnType: typeof(Boolean),
			declaringType: typeof(PlayBasic), defaultValue: false,
			defaultBindingMode: BindingMode.TwoWay,
			propertyChanged: IsAwayPropertyChanged);

		public bool IsAway
		{
			get { return (Boolean)GetValue(IsAwayProperty); }
			set { base.SetValue(IsAwayProperty, value); }
		}

		private static void IsAwayPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (PlayBasic)bindable;
			control.awayBar.IsVisible = (bool)newValue;
            control.notHomeBar.IsVisible = (bool)newValue;
		}

        public static readonly BindableProperty IsActiveProperty = BindableProperty.Create(
			propertyName: "IsActive", returnType: typeof(Boolean),
			declaringType: typeof(PlayBasic), defaultValue: false,
			defaultBindingMode: BindingMode.TwoWay,
			propertyChanged: IsActivePropertyChanged);

		public bool IsActive
		{
			get { return (Boolean)GetValue(IsActiveProperty); }
			set { base.SetValue(IsActiveProperty, value); }
		}

		private static void IsActivePropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (PlayBasic)bindable;
			//control.title.TextColor = control.BGColor;
			//control.BackgroundColor = control.TextColor;
		}

		public PlayBasic()
		{
			InitializeComponent();
		}


	}
}
