﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HostedMobile
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelectorLabel : StackLayout
	{
		public static readonly BindableProperty TitleTextProperty = BindableProperty.Create(
														 propertyName: "TitleText",
														 returnType: typeof(string),
														 declaringType: typeof(SelectorLabel),
														 defaultValue: "",
														 defaultBindingMode: BindingMode.TwoWay,
														 propertyChanged: TitleTextPropertyChanged);

		public string TitleText
		{
			get { return base.GetValue(TitleTextProperty).ToString(); }
			set { base.SetValue(TitleTextProperty, value); }
		}

		private static void TitleTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (SelectorLabel)bindable;
            if (newValue != null)
            {
                control.title.Text = newValue.ToString();
            }
		}

		public static readonly BindableProperty TextColorProperty = BindableProperty.Create(
														propertyName: "TextColor",
                                                        returnType: typeof(Color),
														declaringType: typeof(SelectorLabel),
                                                        defaultValue: Color.Default,
														defaultBindingMode: BindingMode.TwoWay,
														propertyChanged: TextColorPropertyChanged);

		public Color TextColor
		{
            get { return (Color)GetValue(TextColorProperty); }
			set { base.SetValue(TextColorProperty, value); }
		}

		private static void TextColorPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (SelectorLabel)bindable;
			control.title.TextColor = control.TextColor;
			control.title.FontAttributes = FontAttributes.None;
			control.topLine.Color = control.BGColor;
			control.bottomLine.Color = control.BGColor;
			control.leftLine.Color = control.BGColor;
			control.rightLine.Color = control.BGColor;
            control.BackgroundColor = control.BGColor;
		}

		public static readonly BindableProperty BGColorProperty = BindableProperty.Create(
														propertyName: "BGColor",
														returnType: typeof(Color),
														declaringType: typeof(SelectorLabel),
														defaultValue: Color.Default,
														defaultBindingMode: BindingMode.TwoWay,
														propertyChanged: BackgroundColorPropertyChanged);

		public Color BGColor
		{
			get { return (Color)GetValue(BGColorProperty); }
			set { base.SetValue(BGColorProperty, value); }
		}

		private static void BackgroundColorPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (SelectorLabel)bindable;
   //         control.BackgroundColor = (Color)newValue;
			//control.BGColor = (Color)newValue;
		}

		public static readonly BindableProperty FocusTextColorProperty = BindableProperty.Create(
														propertyName: "FocusTextColor",
														returnType: typeof(Color),
														declaringType: typeof(SelectorLabel),
														defaultValue: Color.Default,
														defaultBindingMode: BindingMode.TwoWay,
														propertyChanged: FocusTextColorPropertyChanged);

		public Color FocusTextColor
		{
			get { return (Color)GetValue(FocusTextColorProperty); }
			set { base.SetValue(FocusTextColorProperty, value); }
		}

		private static void FocusTextColorPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (SelectorLabel)bindable;
		}

		public static readonly BindableProperty FocusBGColorProperty = BindableProperty.Create(
														propertyName: "FocusBGColor",
														returnType: typeof(Color),
														declaringType: typeof(SelectorLabel),
														defaultValue: Color.Default,
														defaultBindingMode: BindingMode.TwoWay,
														propertyChanged: FocusBackgroundColorPropertyChanged);

		public Color FocusBGColor
		{
			get { return (Color)GetValue(FocusBGColorProperty); }
			set { base.SetValue(FocusBGColorProperty, value); }
		}

		private static void FocusBackgroundColorPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (SelectorLabel)bindable;
		}

        public static readonly BindableProperty IsActiveProperty = BindableProperty.Create(
                                                        propertyName: "IsActive",
                                                        returnType: typeof(Boolean),
                                                        declaringType: typeof(SelectorLabel),
                                                        defaultValue: false,
														defaultBindingMode: BindingMode.TwoWay,
														propertyChanged: IsActivePropertyChanged);

        public bool IsActive
		{
            get { return (Boolean)GetValue(IsActiveProperty); }
			set { base.SetValue(IsActiveProperty, value); }
		}

		private static void IsActivePropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (SelectorLabel)bindable;
            if ((bool)newValue == true)
            {
                control.title.TextColor = control.FocusTextColor;
                control.title.FontAttributes = FontAttributes.Bold;
                control.topLine.Color = control.FocusBGColor;
                control.bottomLine.Color = control.FocusBGColor;
                control.leftLine.Color = control.FocusBGColor;
                control.rightLine.Color = control.FocusBGColor;
				control.BackgroundColor = control.FocusBGColor;
			}
                else
			{
				control.title.TextColor = control.TextColor;
                control.title.FontAttributes = FontAttributes.None;
				control.topLine.Color = control.BGColor;
				control.bottomLine.Color = control.BGColor;
				control.leftLine.Color = control.BGColor;
				control.rightLine.Color = control.BGColor;
				control.BackgroundColor = control.BGColor;
			}
        }

		public SelectorLabel()
		{
			InitializeComponent();
		}


	}
}