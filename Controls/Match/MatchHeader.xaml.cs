﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HostedMobile
{
    public partial class MatchHeader : StackLayout
    {
        public MatchHeader()
        {
            InitializeComponent();
        }

		async void CloseMatch(object sender, EventArgs args)
		{
			await Navigation.PopModalAsync();
		}

	}
}
